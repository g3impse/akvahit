import jQuery from "jquery"
/* eslint-disable */

jQuery( document ).ready( function( $ )
{
    const range = $( "#priceRange" )
    const label = $( ".choosenAmount" )
    const cashback = $( ".calculator__form--price .amount" )

    const calc_items = $( ".calculator__items .calculator__items--item-info" )
    const coffee = $( calc_items[ 0 ] ).find( "span" )
    const car = $( calc_items[ 1 ] ).find( "span" )
    const fly = $( calc_items[ 2 ] ).find( "span" )
    const iphone = $( calc_items[ 3 ] ).find( "span" )
    const cinema = $( calc_items[ 4 ] ).find( "span" )

    label.html( range.val() )
    range.on( "input", function()
    {
        label.html( this.value )

        const cashback_sum = Math.floor( this.value / 100 * 6 )
        cashback.html( cashback_sum )

        coffee.html( Math.floor( cashback_sum / 250 ) )
        car.html( Math.floor( cashback_sum / 1500 ) )
        fly.html( Math.floor( cashback_sum / 10000 ) )
        iphone.html( Math.floor( cashback_sum / 70000 ) )
        cinema.html( Math.floor( cashback_sum / 400 ) )
    } )
    range.trigger( "input" )

    const form = $( "#stat-partnerom form" )
    const modal_container = $( "#modal-container" )

    const btn = form.find( "button[type=\"submit\"]" )
    btn.click( function()
    {
        for ( const el of form.find( "input" ) )
        {
            if ( el.name == "iambot" || el.name == "patronymic" ) { continue }
            el.required = true
        }
    } )

    form.submit( function( e )
    {
        e.stopPropagation()
        e.preventDefault()

        const btn = $( this ).find( "button[type=\"submit\"]" )
        btn.disabled = true

        $.ajax( {
            url   : "mail.php",
            method: "post",
            data  : $( this ).serialize(),
            success( data )
            {
                if ( data == "ok" )
                {
                    modal_container.css( "display", "" )
                    modal_container.animate( {
                        opacity: 1
                    }, 200 )
                }
                else
                {
                    alert( data )
                }

                btn.disabled = false
            },
            error( err )
            {
                btn.disabled = false
            }
        } )
    } )

    function hide_modal( e )
    {
        e.stopPropagation()
        e.preventDefault()

        for ( const el of form.find( "input" ) )
        {
            el.value = ""
            el.checked = false
            el.required = false
        }

        modal_container.animate( {
            opacity: 0
        }, 200, function()
        {
            modal_container.css( "display", "none" )
        } )
    }
    modal_container.find( "a.btn, a.close" ).click( hide_modal )

    const $hamburger = $( ".hamburger" )
    $hamburger.on( "click", function( e )
    {
        $hamburger.toggleClass( "is-active" )
        // Do something else, like open/close menu
    } )

    $( "path" ).hover( function( e )
    {
        $( "path" ).css( "fill", "#fff" )
        $( ".indicator" ).html( "" )
        const id = $( this ).attr( "id" ).toUpperCase()

        if ( $( this ).attr( "name" ) )
        {
            const name = $( this ).attr( "name" )
            $( "<div><span>" + id.substr( 0, 2 ) + "</span> " + name + "</div>" ).appendTo( ".indicator" )
        }

        $( ".change" ).remove()

        const script = document.createElement( "script" )

        document.body.appendChild( script )

        $( this ).css( "fill", "#ff3e3d" )
        $( "path" ).not( this ).css( "fill", "rgba(255,255,255,1)" )
        $( ".indicator" ).css( { top: e.pageY, left: e.pageX + 30 } ).show()
    }, function()
    {
        $( ".indicator" ).html( "" )
        $( ".indicator" ).hide()
        $( "path" ).css( "fill", "rgba(255,255,255,1)" )
    } )

    const idAarr = [ "RU-MOW", "RU-SPE", "RU-NEN", "RU-YAR", "RU-CHE", "RU-ULY", "RU-TYU", "RU-TUL", "RU-SVE", "RU-RYA", "RU-ORL", "RU-OMS", "RU-NGR", "RU-LIP", "RU-KRS", "RU-KGN", "RU-KGD", "RU-IVA", "RU-BRY", "RU-AST", "RU-KHA", "RU-CE", "RU-UD", "RU-SE", "RU-MO", "RU-KR", "RU-KL", "RU-IN", "RU-AL", "RU-BA", "RU-AD", "RU-CR", "RU-SEV", "RU-KO", "RU-KIR", "RU-PNZ", "RU-TAM", "RU-MUR", "RU-LEN", "RU-VLG", "RU-KOS", "RU-PSK", "RU-ARK", "RU-YAN", "RU-CHU", "RU-YEV", "RU-TY", "RU-SAK", "RU-AMU", "RU-BU", "RU-KK", "RU-KEM", "RU-NVS", "RU-ALT", "RU-DA", "RU-STA", "RU-KB", "RU-KC", "RU-KDA", "RU-ROS", "RU-SAM", "RU-TA", "RU-ME", "RU-CU", "RU-NIZ", "RU-VLA", "RU-MOS", "RU-KLU", "RU-BEL", "RU-ZAB", "RU-PRI", "RU-KAM", "RU-MAG", "RU-SA", "RU-KYA", "RU-ORE", "RU-SAR", "RU-VGG", "RU-VOR", "RU-SMO", "RU-TVE", "RU-PER", "RU-KHM", "RU-TOM", "RU-IRK" ]
    const idAarr2 = new Array(
        [ "KZ-AKM", "Aqmola oblysy" ],
        [ "KZ-ALM", "Aqtöbe oblysy" ],
        [ "KZ-ALM", "Almaty oblysy" ],
        [ "KZ-ATY", "Atyraū oblysy" ],
        [ "KZ-KAR", "Qaraghandy oblysy" ],
        [ "KZ-KUS", "Qostanay oblysy" ],
        [ "KZ-KZY", "Qyzylorda oblysy" ],
        [ "KZ-MAN", "Mangghystaū oblysy" ],
        [ "KZ-PAV", "Pavlodar oblysy" ],
        [ "KZ-SEV", "Soltüstik Qazaqstan oblysy" ],
        [ "KZ-VOS", "Shyghys Qazaqstan oblysy" ],
        [ "KZ-YUZ", "Ongtüstik Qazaqstan oblysy" ],
        [ "KZ-ZAP", "Batys Qazaqstan oblysy" ],
        [ "KZ-ZHA", "Zhambyl oblysy" ],
        [ "KZ-AS", "Aral Sea" ],
        [ "KZ-AKT", "Aqtöbe oblysy" ],

        [ "RU-MOW", "Москва", "moscow.gif" ],
        [ "RU-CHE", "Челябинская область", "chelyabinskaya_flag.png" ],
        [ "RU-ORL", "Орловская область" ],
        [ "RU-OMS", "Омская область", "flag_omskoj_oblasti.png" ],
        [ "RU-LIP", "Липецкая область", "lipeckya.jpg" ],
        [ "RU-KRS", "Курская область", "flag_of_kursk_oblast.png" ],
        [ "RU-RYA", "Рязанская область", "ryazan.png" ],
        [ "RU-BRY", "Брянская область", "bryanskaya_flag.png" ],
        [ "RU-KIR", "Кировская область", "flag_kirovskoy_oblasti.png" ],
        [ "RU-ARK", "Архангельская область", "" ],
        [ "RU-MUR", "Мурманская область", "" ],
        [ "RU-SPE", "Санкт-Петербург", "" ],
        [ "RU-YAR", "Ярославская область", "" ],
        [ "RU-ULY", "Ульяновская область", "" ],
        [ "RU-NVS", "Новосибирская область", "" ],
        [ "RU-TYU", "Тюменская область", "" ],
        [ "RU-SVE", "Свердловская область", "" ],
        [ "RU-NGR", "Новгородская область", "" ],
        [ "RU-KGN", "Курганская область", "" ],
        [ "RU-KGD", "Калининградская область", "" ],
        [ "RU-IVA", "Ивановская область", "" ],
        [ "RU-AST", "Астраханская область", "" ],
        [ "RU-KHA", "Хабаровский край", "" ],
        [ "RU-CE", "Чеченская республика", "" ],
        [ "RU-UD", "Удмуртская республика", "" ],
        [ "RU-SE", "Республика Северная Осетия", "" ],
        [ "RU-MO", "Республика Мордовия", "" ],
        [ "RU-KR", "Республика  Карелия", "" ],
        [ "RU-KL", "Республика  Калмыкия", "" ],
        [ "RU-IN", "Республика  Ингушетия", "" ],
        [ "RU-AL", "Республика Алтай", "" ],
        [ "RU-BA", "Республика Башкортостан", "" ],
        [ "RU-AD", "Республика Адыгея", "" ],
        [ "RU-CR", "Республика Крым", "" ],
        [ "RU-SEV", "Севастополь", "" ],
        [ "RU-KO", "Республика Коми", "" ],
        [ "RU-PNZ", "Пензенская область", "" ],
        [ "RU-TAM", "Тамбовская область", "" ],
        [ "RU-LEN", "Ленинградская область", "" ],
        [ "RU-VLG", "Вологодская область", "" ],
        [ "RU-KOS", "Костромская область", "" ],
        [ "RU-PSK", "Псковская область", "" ],
        [ "RU-YAN", "Ямало-Ненецкий АО", "" ],
        [ "RU-CHU", "Чукотский АО", "" ],
        [ "RU-YEV", "Еврейская автономская область", "" ],
        [ "RU-TY", "Республика Тыва", "" ],
        [ "RU-SAK", "Сахалинская область", "" ],
        [ "RU-AMU", "Амурская область", "" ],
        [ "RU-BU", "Республика Бурятия", "" ],
        [ "RU-KK", "Республика Хакасия", "" ],
        [ "RU-KEM", "Кемеровская область", "" ],
        [ "RU-ALT", "Алтайский край", "" ],
        [ "RU-DA", "Республика Дагестан", "" ],
        [ "RU-KB", "Кабардино-Балкарская республика", "" ],
        [ "RU-KC", "Карачаевая-Черкесская республика", "" ],
        [ "RU-KDA", "Краснодарский край", "" ],
        [ "RU-ROS", "Ростовская область", "" ],
        [ "RU-SAM", "Самарская область", "" ],
        [ "RU-TA", "Республика Татарстан", "" ],
        [ "RU-ME", "Республика Марий Эл", "" ],
        [ "RU-CU", "Чувашская республика", "" ],
        [ "RU-NIZ", "Нижегородская край", "" ],
        [ "RU-VLA", "Владимировская область", "" ],
        [ "RU-MOS", "Московская область", "" ],
        [ "RU-KLU", "Калужская область", "" ],
        [ "RU-BEL", "Белгородская область", "" ],
        [ "RU-ZAB", "Забайкальский край", "" ],
        [ "RU-PRI", "Приморский край", "" ],
        [ "RU-KAM", "Камачатский край", "" ],
        [ "RU-MAG", "Магаданская область", "" ],
        [ "RU-SA", "Республика Саха", "" ],
        [ "RU-KYA", "Красноярский край", "" ],
        [ "RU-ORE", "Оренбургская область", "" ],
        [ "RU-SAR", "Саратовская область", "" ],
        [ "RU-VGG", "Волгоградская область", "" ],
        [ "RU-VOR", "Ставропольский край", "" ],
        [ "RU-SMO", "Смоленская область", "" ],
        [ "RU-TVE", "Тверская область", "" ],
        [ "RU-PER", "Пермская область", "" ],
        [ "RU-KHM", "Ханты-Мансийский АО", "" ],
        [ "RU-KHM", "Ханты-Мансийский АО", "" ],
        [ "RU-TOM", "Томская область", "" ],
        [ "RU-IRK", "Иркутская область", "" ],
        [ "RU-NEN", "Ненецскй АО", "" ],
        [ "RU-STA", "Ставропольский край", "" ],
        [ "RU-TUL", "Тульская область", "tulskaya_flag.png" ]

    )

    $( "path" ).each( function()
    {
        const regId = $( this ).attr( "id" )
        let flag = ""
        let name = ""
        for ( let j = 0; j < idAarr2.length; j++ )
        {
            if ( regId == idAarr2[ j ][ 0 ] )
            {
                name = idAarr2[ j ][ 1 ]
                flag = "flags/" + idAarr2[ j ][ 2 ]

                $( this ).attr( "name", name )
                $( this ).attr( "flag", flag )
            }
        }

        const regIdDiv = "<div class=\"reg\" >" + "[" + "<span>" + regId + "</span>" + "]" + " " + name + "</div>"
        $( regIdDiv ).appendTo( ".regs" )
    } )

    function naming()
    {

    }

    $( ".reg" ).hover( function( e )
    {
        const id = $( this ).find( "span" ).text()
        idHover = "#" + id
        $( idHover ).css( "fill", "#ff3e3d" )
    }, function()
    {
        $( ".indicator" ).html( "" )
        $( ".indicator" ).hide()
        $( "path" ).css( "fill", "rgba(0,0,0,0.2)" )
    } )
} )
