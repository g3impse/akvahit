/**
 * Created by user on 20.09.2016.
 */

$( document ).ready( function()
{
// ��������
    $( ".inp-your-tel" ).mask( "+7 (000) 000-00-00" )

    $( ".parallax-window" ).parallax( { imageSrc: "images/bg_banner.png" } )
    $( ".parallax-window2" ).parallax( { imageSrc: "images/bg_bl_3.jpg" } )
    $( ".block_3" ).parallax( { imageSrc: "images/bg_block_3.jpg" } )
    //        $("a[href*='#']").mPageScroll2id();

    // ���� ��������������
    $( "#sandwich, .menu_item" ).click( function()
    {
        $( "#sandwich" ).toggleClass( "active" )
        if ( $( "#secondary" ).is( ":visible" ) ) { $( "#secondary" ).fadeOut( 600 ) }
        else { $( "#secondary" ).fadeIn( 600 ) }
    } )

    // �������� ������ �������� ����� #1
    $( "#btn_submit" ).click( function()
    {
        // �������� ������ � �����
        const name 	 = $( "#user_name" ).val()
        const phone 	 = $( "#user_tel" ).val()

        const json = {
            code: "ustanovka-schetchika-tepla",
            data: {
                name,
                phone
            }
        }

        // ���������� ������
        $.ajax( {
            url        : "/api/forms/send",
            type       : "post",
            contentType: "application/json",
            dataType   : "json",
            data       : JSON.stringify( json ),
            // ����� ��������� ������ �������
            success( data )
            {
                $( ".messages" ).html( data.resulterr ) // ������� ����� �������
                if ( $( "#user_name" ).val() !== "" & $( "#user_tel" ).val() !== "" & data.status !== false )
                {
                    $( "#form" )[ 0 ].reset()
                    $( "#myModal" ).modal( "show" )
                }
            }
        } )
    } )

    // �������� ������ �������� ����� #2
    $( "#btn_submit2" ).click( function()
    {
        // �������� ������ � �����
        const name 	 = $( "#user_name2" ).val()
        const phone 	 = $( "#user_tel2" ).val()

        const json = {
            code: "ustanovka-schetchika-tepla",
            data: {
                name,
                phone
            }
        }

        // ���������� ������
        $.ajax( {
            url        : "/api/forms/send",
            type       : "post",
            contentType: "application/json",
            dataType   : "json",
            data       : JSON.stringify( json ),
            // ����� ��������� ������ �������
            success( data )
            {
                $( ".messages2" ).html( data.resulterr ) // ������� ����� �������
                if ( $( "#user_name2" ).val() !== "" & $( "#user_tel2" ).val() !== "" & data.status !== false )
                {
                    console.log( data.status )
                    $( "#form2" )[ 0 ].reset()
                    $( "#myModal2" ).modal( "show" )
                }
            }
        } )
    } )
    // �������� ������ �������� ����� #3
    $( "#btn_submit3" ).click( function()
    {
        // �������� ������ � �����
        const name 	 = $( "#user_name3" ).val()
        const phone 	 = $( "#user_tel3" ).val()

        const json = {
            code: "ustanovka-schetchika-tepla",
            data: {
                name,
                phone
            }
        }

        // ���������� ������
        $.ajax( {
            url        : "/api/forms/send",
            type       : "post",
            contentType: "application/json",
            dataType   : "json",
            data       : JSON.stringify( json ),
            // ����� ��������� ������ �������
            success( data )
            {
                $( ".messages3" ).html( data.resulterr ) // ������� ����� �������
                if ( data.status )
                {
                    $( "#form3 p" ).addClass( "hidden" )
                    $( "#form3" ).html( data.result )
                    $( ".block_4 .modal-body  img" ).attr( "src",
                        "images/men_after_form.png"
                    )
                }
                if ( $( "#user_name3" ).val() !== "" & $( "#user_tel3" ).val() !== "" )
                {
                    $( "#form3" )[ 0 ].reset()
                }
            }
        } )
    } )

    // �������� ������ �������� ����� #4(TopBar)
    $( "#btn_submit4" ).click( function()
    {
        // �������� ������ � �����
        const name 	 = $( "#user_name4" ).val()
        const phone 	 = $( "#user_tel4" ).val()

        const json = {
            code: "ustanovka-schetchika-tepla",
            data: {
                name,
                phone
            }
        }

        // ���������� ������
        $.ajax( {
            url        : "/api/forms/send",
            type       : "post",
            contentType: "application/json",
            dataType   : "json",
            data       : JSON.stringify( json ),
            // ����� ��������� ������ �������
            success( data )
            {
                $( ".messages4" ).html( data.resulterr ) // ������� ����� �������
                if ( data.status )
                {
                    $( "#form4 p" ).addClass( "hidden" )
                    $( "#form4" ).html( data.result )
                    $( "#myModal4 .modal-body  img" ).attr( "src",
                        "images/men_after_form.png"
                    )
                }
                if ( $( "#user_name4" ).val() !== "" & $( "#user_tel4" ).val() !== "" )
                {
                    $( "#form4" )[ 0 ].reset()
                }
            }
        } )
    } )

    // Loader
    $( "#fakeLoader" ).fakeLoader( {

        timeToHide: 1200, // Time in milliseconds for fakeLoader disappear
        zIndex    : 999, // Default zIndex
        spinner   : "spinner2", // Options: 'spinner1', 'spinner2', 'spinner3', 'spinner4', 'spinner5', 'spinner6', 'spinner7'
        bgColor   : "#336699" // Hex, RGB or RGBA colors

    } )
    // Fancybox

    $( ".fancybox" ).fancybox()
} )
