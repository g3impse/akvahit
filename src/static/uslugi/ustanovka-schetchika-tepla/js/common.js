$(document).ready(function(){
	//Popup
	var window_width = $(window).width();
	var window_height = $(window).height();
	var popup_width = $('.popup').width();
	var popup_height = $('.popup').height();
	$('.popup').css({
		'left' : (window_width-popup_width)/2,
		'top' : (window_height-popup_height)/2,
	});
	$('.thanks').css({
		'left' : (window_width-popup_width)/2,
		'top' : (window_height-popup_height)/2,
	});
	$('.recall').click(function(){
		$('.back').fadeIn();
		$('.popup').fadeIn();
		return false;
	});
	$('.close, .back').click(function(){
		$('.popup').fadeOut();
		$('.thanks').fadeOut();
		$('.back').fadeOut();
		return false;
	});
	//Лицензия
	$('.all_docs').slick({
		dots: false,
		infinite: true,
		autoplay: true,
		speed: 300,
		slidesToShow: 4,
		slidesToScroll: 1,
		arrows: true,
		responsive: [
			{
				breakpoint: 1200,
				settings: {
					slidesToShow: 3,
					slidesToScroll: 1
					
				}
			},
			{
				breakpoint: 992,
				settings: {
					slidesToShow: 3,
					slidesToScroll: 1
					
				}
			},
			{
				breakpoint: 767,
				settings: {
					slidesToShow: 1,
					slidesToScroll: 1
					
				}
			}
		]
	});
	//Отзывы
	$('.all_reviews').slick({
		dots: false,
		infinite: true,
		autoplay: true,
		speed: 300,
		slidesToShow: 1,
		arrows: true
	});
	//Вопросы и ответы
	$('.one_question').click(function(){
		
			if($(this).hasClass('active')){
				$(this).removeClass('active');
			}
			else{
				$('.questions .one_question').removeClass('active');
				$(this).addClass('active');
			}
		return false;
	});
	//Отправка данных на почту из форм
	$(document).on('submit', '.send_mail', function(){
		var data = $(this).serialize();
		$.ajax({
			url: '/mail.php',
			type: 'post',
			data: data,
			success: function(res){
				if (res=='success'){
					$('.popup').fadeOut();
					$('.back').fadeIn();
					$('.thanks').fadeIn();
				}
			}
		});
		return false;
	});
});
