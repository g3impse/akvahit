export default ( { get } ) =>
    ( {
        get( params )
        {
            return get( "/api/data-store/get", params )
        },
        getRes( params )
        {
            return get( "/api/data-store/get", params, { results: true } )
        }
    } )
