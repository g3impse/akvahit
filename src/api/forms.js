import { getPathName } from "@/lib/url"

export default axios => ( {

    send( code, data )
    {
        return axios.post( "/api/forms/send", { code, data, url: getPathName() } )
    }

} )
