export default ( { get, post } ) =>
    ( {
        get( params )
        {
            return get( "/api/delivery/calculator", params )
        },

        points( params )
        {
            return get( "/api/delivery/points", params, { all: true } )
        }
    } )
