export default ( { get } ) =>
    ( {
        async get( params )
        {
            const res = await get( "/api/menu", params, { results: true } )
            const menu = res && res[ 0 ] ? res[ 0 ] : {}

            return menu
        }
    } )
