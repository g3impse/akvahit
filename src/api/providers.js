export default ( { get } ) =>
    ( {
        get( params )
        {
            return get( "/api/providers/product-data", params )
        }
    } )
