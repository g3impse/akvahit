const CATEGORIES = "/api/shop/categories"
const MAP = `${CATEGORIES}/map`

export default ( { get } ) =>
    ( {
        get( params )
        {
            return get( CATEGORIES, params )
        },
        getAll( params )
        {
            return get( CATEGORIES, params, { all: true } )
        },
        tree( params )
        {
            return get( "/api/shop/categories/tree", params )
        },
        map( params )
        {
            return get( MAP, params, { all: true } )
        },
        synonyms( params )
        {
            params.type = "seo_synonym"
            return this.getAll( params )
        },
        alternativeTitles( params )
        {
            return get( "/api/meta/category/get", params )
        }
    } )
