export default ( { get } ) =>
    ( {
        primary( params )
        {
            return get( "/api/shop/filter-data-primary", params )
        },
        props( params )
        {
            return get( "/api/shop/filter-data-props", params, { all: true } )
        }
    } )
