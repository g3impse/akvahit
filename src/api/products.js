export default ( { get } ) =>
    ( {
        get( params )
        {
            return get( "/api/shop/products", params )
        },

        getAll( params )
        {
            return get( "/api/shop/products", params, { all: true } )
        },

        random( params )
        {
            return get( "/api/shop/products/random-list", params )
        },

        search( params )
        {
            return get( "/api/shop/search", params )
        },

        map( params )
        {
            return get( "/api/shop/products/map", params )
        },

        abc( params )
        {
            return get( "/api/shop/products/map/abc", params, { all: true } )
        }
    } )
