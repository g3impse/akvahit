export default ( { get, post } ) =>
    ( {
        get( params )
        {
            return get( "/api/orders/get", params )
        },

        create( data )
        {
            return post( "/api/orders/create", data )
        }
    } )
