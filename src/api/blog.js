export default ( { get, post, postFormData, put, del } ) =>
    ( {
        list( type, name, params )
        {
            return get( `/api/blog/${type}/${name}/list`, params )
        },

        listAll( type, name, params )
        {
            return get( `/api/blog/${type}/${name}/list`, params, { all: true } )
        },

        get( type, name, params )
        {
            return get( `/api/blog/${type}/${name}/get/${params.id}`, params )
        },

        create( type, name, params )
        {
            return post( `/api/blog/${type}/${name}/create`, params )
        },

        delete( type, name, params )
        {
            return del( `/api/blog/${type}/${name}/delete/${params.id}`, params )
        },

        update( type, name, params )
        {
            return put( `api/blog/${type}/${name}/update/${params.id}`, params )
        },

        base: {
            list( name, params )
            {
                return get( `/api/blog/${name}/list`, params )
            },

            listAll( name, params )
            {
                return get( `/api/blog/${name}/list`, params, { all: true } )
            },

            get( name, params )
            {
                return get( `/api/blog/${name}/get/${params.id}`, params )
            },

            create( name, params )
            {
                return post( `/api/blog/${name}/create`, params )
            },

            image( name, params )
            {
                return postFormData( `/api/blog/${name}/create`, params )
            },

            delete( name, params )
            {
                return del( `/api/blog/${name}/delete/${params.id}`, params )
            },

            update( name, params )
            {
                return put( `api/blog/${name}/update/${params.id}`, params )
            },

            fields( params )
            {
                return get( "/api/blog/item-types/fields", params )
            }
        }
    } )
