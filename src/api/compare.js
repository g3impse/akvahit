export default ( { get, post } ) =>
    ( {
        get( params )
        {
            return get( "/api/shop/compare", params, { result: true } )
        }
    } )
