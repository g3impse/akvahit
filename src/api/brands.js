export default ( { get } ) =>
    ( {
        get( params )
        {
            return get( "/api/shop/brands", params, { all: true } )
        }
    } )
