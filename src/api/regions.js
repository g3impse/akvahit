export default ( { get, post } ) =>
    ( {
        listAll( params = {} )
        {
            return get( "/api/regions/list", params, { all: true } )
        },

        primary( params = {} )
        {
            params.primary = true
            return get( "/api/regions/list", params, { all: true } )
        },

        get( params )
        {
            return get( "/api/regions/get", params )
        },

        set( data )
        {
            return post( "/api/regions/set", data )
        },

        search( params )
        {
            return get( "/api/regions/search", params, { results: true } )
        },

        addresses( params )
        {
            return get( "/api/regions/address-list", params )
        }
    } )
