module.exports = {
    root: true,
    env : {
        browser: true,
        node   : true
    },
    parserOptions: {
        parser           : "@babel/eslint-parser",
        requireConfigFile: false
    },
    extends: [
        "@nuxtjs",
        "plugin:nuxt/recommended"
    ],
    plugins: [
    ],
    // ignorePatterns: [ "BaseGrid.vue" ],
    // add your custom rules here
    rules: {
        "brace-style"                : [ "error", "allman", { allowSingleLine: true } ],
        // "nuxt/no-cjs-in-config": "off",
        "no-console"                 : process.env.NODE_ENV === "production" ? "error" : "off",
        "no-debugger"                : process.env.NODE_ENV === "production" ? "error" : "off",
        // "vue/html-indent": ["error", "tab"],
        // "vue/html-closing-bracket-newline": "off",
        indent                       : [ "error", 4, { ignoredNodes: [ "TemplateLiteral" ] } ],
        "vue/html-indent"            : [ "error", 4 ],
        "key-spacing"                : [ "error", { align: "colon" } ],
        quotes                       : [ "error", "double" ],
        "space-before-function-paren": [ "error", "never" ],
        "no-multi-spaces"            : [ "error", { ignoreEOLComments: true, exceptions: { VariableDeclarator: true, ImportDeclaration: true } } ],
        "space-in-parens"            : [ "error", "always" ],
        "object-curly-spacing"       : [ "error", "always" ],
        "array-bracket-spacing"      : [ "error", "always" ],
        "computed-property-spacing"  : [ "error", "always" ],
        "template-curly-spacing"     : "off"
    }
}
