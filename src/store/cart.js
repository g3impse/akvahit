import Vue from "vue"
import { arrayToObject, objectToArray } from "@/lib/utils"
import { getTotalQuantity, getTotalCost } from "@/lib/cart"
import { setTimer } from "@/lib/timer"
import { CART } from "@/lib/timer/const"

const state = () =>
{
    return {
        cart: null
    }
}

const getters = {
    totalQuantity: ( state ) =>
    {
        return getTotalQuantity( state.cart )
    }
}

const mutations = {
    setCart( state, data )
    {
        data.positions = arrayToObject( data.positions, "id" )
        state.cart = data
    },

    addToCart( state, product )
    {
        Vue.set( state.cart.positions, product.id, product )
    },

    removeFromCart( state, product )
    {
        Vue.delete( state.cart.positions, product.id )
    },

    updateQuantity( state, data )
    {
        const { id, quantity } = data
        state.cart.positions[ id ].quantity = quantity
    },

    clearCart( state )
    {
        state.cart.positions = {}
    },

    recalcCart( state )
    {
        state.cart.totalCost = getTotalCost( state.cart )
    }
}

const actions = {
    async getCart( { commit } )
    {
        const res = await this.$api.cart.get()
        commit( "setCart", res )
    },

    setCart( { commit, state } )
    {
        commit( "recalcCart" )
        setTimer( CART, async() =>
        {
            const positions = objectToArray( state.cart.positions )
            const res = await this.$api.cart.set( { positions } )
            commit( "setCart", res )
        } )
    },

    addToCart( { commit, dispatch }, payload )
    {
        commit( "addToCart", payload )
        dispatch( "setCart" )
    },

    removeFromCart( { commit, dispatch }, payload )
    {
        commit( "removeFromCart", payload )
        dispatch( "setCart" )
    },

    updateQuantity( { commit, dispatch, state }, payload )
    {
        commit( "updateQuantity", payload )
        dispatch( "setCart" )
    },

    clearCart( { commit, dispatch } )
    {
        commit( "clearCart" )
        dispatch( "setCart" )
    }
}

export default {
    state,
    getters,
    mutations,
    actions
}
