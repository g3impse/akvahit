import { getName } from "@/lib/cookie"
import { arrayToObject, tryAR } from "@/lib/utils"
import { prepositionV } from "@/lib/namings"
import { urlToCity } from "@/lib/url"

const regionId = getName( "regionId" )

const defaultRegion = { id: 1, title: "Москва", titleCases: { sg: { base: "Москва", gen: "Москвы", dat: "Москве", acc: "Москву", ins: "Москвой", pre: "Москве" }, pl: { base: null, gen: null, dat: null, acc: null, ins: null, pre: null } }, slug: "msk", isDefault: true, primary: true, kladrId: "7700000000000", isoCode: "RU-MOW", fiasId: "0c5b2444-70a0-4932-980c-b4dc0d3f02b5", faked: false, isSet: false }

export const state = () => ( {
    primary: [],
    all    : {},
    region : null
} )

export const getters = {
    inRegion( state )
    {
        return state.region && !state.region.faked && !state.region.isDefault ? prepositionV( state.region.titleCases.sg.pre || state.region.title ) : ""
    },

    regionSlug( state )
    {
        return state.region?.faked || state.region?.isDefault ? "" : state.region?.slug
    },

    regionPre( state )
    {
        return prepositionV( state.region.titleCases?.sg?.pre || state.region.title )
    },

    regionAcc( state )
    {
        return prepositionV( state.region.titleCases?.sg?.acc || state.region.title )
    }
}

export const mutations = {
    setPrimaryRegions( state, regions )
    {
        state.primary = regions
    },

    setAllRegions( state, regions )
    {
        state.all = arrayToObject( regions, "slug" )
    },

    setRegion( state, region )
    {
        state.region = region
    }
}

export const actions = {
    async initRegions( { state, dispatch, commit }, route )
    {
        await dispatch( "getPrimaryRegions" )
        await dispatch( "getAllRegions" )
        await dispatch( "getRegion", route )
    },

    async getPrimaryRegions( { commit } )
    {
        const regions = await this.$api.regions.primary()
        commit( "setPrimaryRegions", regions )
    },

    async getAllRegions( { commit } )
    {
        const regions = await this.$api.regions.listAll()
        commit( "setAllRegions", regions )
    },

    async getRegion( { rootState, state, commit, dispatch }, route )
    {
        // Region from cookie -> index plugin.
        if ( rootState.cookie[ regionId ] )
        {
            const res = await tryAR( () => this.$api.regions.get( { id: rootState.cookie[ regionId ] } ), () => this.$api.regions.get() )

            if ( res.id === rootState.cookie[ regionId ] ) // Cookie is actual.
            {
                res.isSet = true // To match client side after setRegion action.
            }

            commit( "setRegion", res )
        }
        else // From url.
        {
            const city = urlToCity( route.path )
            const res = await tryAR( () => this.$api.regions.get(), () => defaultRegion )
            commit( "setRegion", state.all[ city ] || res )
        }
    },

    async setRegion( { state, commit, dispatch }, region )
    {
        const res = await this.$api.regions.set( region )
        commit( "setRegion", res )
        dispatch( "cookie/setCookie", { name: regionId, value: res.id }, { root: true } )
    }
}
