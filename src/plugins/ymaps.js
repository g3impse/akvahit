import Vue from "vue"
import YmapPlugin from "vue-yandex-maps"

const settings = {
    apiKey    : "29a4b68a-cc9c-4dd1-9203-878c49a20f07",
    lang      : "ru_RU",
    coordorder: "latlong",
    enterprise: false,
    version   : "2.1"
}

Vue.use( YmapPlugin, settings )
