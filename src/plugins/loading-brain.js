import Vue from "vue"
import { isFastLoad, getConst } from "@/lib/url"

Vue.mixin( {
    beforeRouteLeave( to, from, next )
    {
        if ( isFastLoad( getConst( to.path ) ) )
        {
            this.$store.commit( "setRouterLoadingEnable", false )
        }
        next()
    }
} )
