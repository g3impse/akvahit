import { getName } from "@/lib/cookie"

const regionId = getName( "regionId" )

export default async function( { store } ) // , query } )
{
    const reqs = []

    if ( !store.state.regions.region.isSet )
    {
        if ( store.state.cookie[ regionId ] )
        {
            store.dispatch( "cookie/deleteCookie", { name: regionId } )
        }
        else
        {
            reqs.push( store.dispatch( "regions/setRegion", store.state.regions.region ) )
        }
    }

    if ( reqs.length > 0 )
    {
        await Promise.all( reqs )
    }
}
