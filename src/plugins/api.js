import Address from "@/api/address"
import Blog from "@/api/blog"
import Brand from "@/api/brand"
import Brands from "@/api/brands"
import Cart from "@/api/cart"
import Categories from "@/api/categories"
import Category from "@/api/category"
import Comments from "@/api/comments"
import Compare from "@/api/compare"
import Core from "@/api/core"
import Delivery from "@/api/delivery"
import Filter from "@/api/filter"
import Forms from "@/api/forms"
import Menu from "@/api/menu"
import Meta from "@/api/meta"
import Orders from "@/api/orders"
import Product from "@/api/product"
import Products from "@/api/products"
import Providers from "@/api/providers"
import Regions from "@/api/regions"
import Store from "@/api/store"
import Time from "@/api/time"

import { init } from "@/lib/api"

export default ( { $axios }, inject ) =>
{
    // Initialize basic API functions.
    const { get, post, postFormData, put, del } = init( { axios: $axios } )
    const api = { get, post, postFormData, put, del }

    // Initialize API factories
    const factories = {
        address   : Address( api ),
        blog      : Blog( api ),
        brand     : Brand( api ),
        brands    : Brands( api ),
        cart      : Cart( api ),
        categories: Categories( api ),
        category  : Category( api ),
        comments  : Comments( api ),
        compare   : Compare( api ),
        core      : Core( api ),
        delivery  : Delivery( api ),
        filter    : Filter( api ),
        forms     : Forms( api ),
        menu      : Menu( api ),
        meta      : Meta( api ),
        orders    : Orders( api ),
        product   : Product( api ),
        products  : Products( api ),
        providers : Providers( api ),
        regions   : Regions( api ),
        store     : Store( api ),
        time      : Time( api )
    }

    // Inject $api
    inject( "api", factories )
}
