import { getUrl, isUrl } from "@/lib/url"

export default ( context, inject ) =>
{
    inject( "getUrl", getUrl )
    inject( "isUrl", isUrl )
}
