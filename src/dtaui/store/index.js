import { cleanPhone } from "@/lib/namings"
import { getName } from "@/lib/cookie"
const favorite = getName( "favorite" )
const compare = getName( "compare" )

export const state = () =>
{
    return {
        phoneMain          : null,
        phoneRegional      : null,
        emailMain          : null,
        addressMain        : null,
        menuCatalog        : null,
        menuMain           : null,
        footerMenuCatalog  : null,
        footerMenuSite     : null,
        routerLoadingEnable: true
    }
}

// TODO: sort out lib ideas.
export const getters =
    {
        phone( state )
        {
            return !state.regions?.region?.isDefault ? state.phoneRegional : state.phoneMain
        },

        cleanPhone( _, getters )
        {
            return cleanPhone( getters.phone )
        },

        email( state )
        {
            return state.emailMain
        },

        address( state )
        {
            return state.addressMain
        }
    }

export const actions = {
    async nuxtServerInit( { dispatch }, { req, route } )
    {
        await dispatch( "initContacts" )
        await dispatch( "initMenus" )
        await dispatch( "regions/initRegions", route )
        await dispatch( "cart/getCart" )
        await dispatch( "cookie/initCookies", req )
    },

    async initContacts( { commit } )
    {
        const res = await this.$api.store.getRes( { group: "contacts" } )

        commit( "setPhoneMain", res.find( x => x.code === "phone-main" )?.content || "" )
        commit( "setPhoneRegional", res.find( x => x.code === "phone-regional" )?.content || "" )
        commit( "setEmailMain", res.find( x => x.code === "email-main" )?.content || "" )
        commit( "setAddressMain", res.find( x => x.code === "address-main" )?.content || "" )
    },

    async initMenus( { commit } )
    {
        const menuCatalog = await this.$api.menu.get( { code: "menu-catalog" } )
        const menuMain = await this.$api.menu.get( { code: "menu-main" } )
        const footerMenuCatalog = await this.$api.menu.get( { code: "footer-catalog" } )
        const footerMenuSite = await this.$api.menu.get( { code: "footer-site" } )
        commit( "setMenuCatalog", menuCatalog )
        commit( "setMenuMain", menuMain )
        commit( "setFooterMenuCatalog", footerMenuCatalog )
        commit( "setFooterMenuSite", footerMenuSite )
    },

    toggleFavorite( { rootState, commit }, product )
    {
        if ( rootState.cookie.favorite[ product.id ] )
        {
            commit( "cookie/deleteCookieKey", { name: favorite, key: product.id } )
        }
        else
        {
            commit( "cookie/setCookieKey", { name: favorite, key: product.id, value: true } )
        }
    },

    toggleCompare( { rootState, commit }, product )
    {
        if ( rootState.cookie.compare[ product.id ] )
        {
            commit( "cookie/deleteCookieKey", { name: compare, key: product.id } )
        }
        else
        {
            commit( "cookie/setCookieKey", { name: compare, key: product.id, value: true } )
        }
    }
}

export const mutations = {
    setPhoneMain( state, value )
    {
        state.phoneMain = value
    },

    setPhoneRegional( state, value )
    {
        state.phoneRegional = value
    },

    setEmailMain( state, value )
    {
        state.emailMain = value
    },

    setAddressMain( state, value )
    {
        state.addressMain = value
    },

    setMenuCatalog( state, object )
    {
        state.menuCatalog = object
    },

    setMenuMain( state, object )
    {
        state.menuMain = object
    },

    setFooterMenuCatalog( state, object )
    {
        state.footerMenuCatalog = object
    },

    setFooterMenuSite( state, object )
    {
        state.footerMenuSite = object
    },

    setRouterLoadingEnable( state, payload )
    {
        state.routerLoadingEnable = payload
    }
}
