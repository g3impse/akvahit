import { getName, getLocalStorage, setLocalStorage } from "@/lib/localStorage"
import { arrayToObject, tryR } from "@/lib/utils"
import { prepositionV } from "@/lib/namings"
import { urlToCity } from "@/lib/url"

const city = getName( "city" )

export const state = () => ( {
    primary      : [],
    all          : [],
    region       : null,
    defaultRegion: { id: 1, title: "Москва", titleCases: { sg: { base: "Москва", gen: "Москвы", dat: "Москве", acc: "Москву", ins: "Москвой", pre: "Москве" }, pl: { base: null, gen: null, dat: null, acc: null, ins: null, pre: null } }, slug: "msk", isDefault: true, primary: true, kladrId: "7700000000000", isoCode: "RU-MOW", fiasId: "0c5b2444-70a0-4932-980c-b4dc0d3f02b5", faked: false, isSet: false }
} )

export const getters = {
    inRegion( state )
    {
        return state.region && !state.region.faked && !state.region.isDefault ? prepositionV( state.region.titleCases.sg.pre || state.region.title ) : ""
    },

    regionSlug( state )
    {
        return state.region?.faked || state.region?.isDefault ? "" : state.region?.slug
    },

    regionPre( state )
    {
        return prepositionV( state.region?.titleCases?.sg?.pre || state.region?.title )
    },

    regionAcc( state )
    {
        return prepositionV( state.region?.titleCases?.sg?.acc || state.region?.title )
    }
}

export const mutations = {
    setPrimaryRegions( state, regions )
    {
        state.primary = regions
    },

    setAllRegions( state, regions )
    {
        state.all = arrayToObject( regions, "slug" )
    },

    setRegion( state, region )
    {
        state.region = region
    }
}

export const actions = {
    async initRegions( { state, dispatch }, route )
    {
        await dispatch( "getPrimaryRegions" )
        await dispatch( "getAllRegions" )
        await dispatch( "getRegion" )

        const city = urlToCity ? urlToCity( route.path ) : null

        if ( state.all[ city ] && !state.region?.isSet )
        {
            await dispatch( "setRegion", state.all[ city ] )
        }
    },

    async getPrimaryRegions( { commit } )
    {
        const regions = await this.$api.regions.primary()
        commit( "setPrimaryRegions", regions )
    },

    async getAllRegions( { commit } )
    {
        const regions = await this.$api.regions.listAll()
        commit( "setAllRegions", regions )
    },

    async getRegion( { state, commit } )
    {
        await tryR( () =>
        {
            const res = getLocalStorage( { name: city } )
            commit( "setRegion", res )
        }, async() =>
        {
            const res = await this.$api.regions.get()
            commit( "setRegion", res || state.defaultRegion )
        } )
    },

    async setRegion( { state, commit }, region )
    {
        const res = await this.$api.regions.set( region )
        commit( "setRegion", res )
        setLocalStorage( { name: city, value: state.region } )
    }
}
