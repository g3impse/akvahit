import Vue from "vue"

import { setCookie, getCookie, ifExists, initState, addDefaultOptions } from "@/lib/cookie"
import { JSON, STRING } from "@/lib/cookie/const"

import * as cookies from "@/lib/cookie/cookies"

export const state = () =>
{
    return {
        ...initState( cookies ),
        init: false // True after the initCookies mutation.
    }
}

export const mutations =
{
    // Used in nuxtServerInit. NOTE: Server-side only.
    initCookies( state, req )
    {
        Object.keys( cookies ).forEach( ( cookie ) =>
        {
            state[ cookie ] = getCookie( req?.headers /* window.document */, { name: cookie, type: cookies[ cookie ] } )
        } )
        state.init = true
    },

    // Completely overwrites.
    setCookie( state, cookie = {} )
    {
        const { name, value, type, days } = addDefaultOptions( cookie )

        ifExists( name, state, () =>
        {
            state[ name ] = value
            setCookie( window.document, { name, value, type, days } )
        } )
    },

    // Sets object entrance.
    setCookieKey( state, cookie = {} )
    {
        const { name, key, value, days } = addDefaultOptions( cookie )

        ifExists( name, state, () =>
        {
            Vue.set( state[ name ], key, value )
            setCookie( window.document, { name, value: state[ name ], days, type: JSON } )
        } )
    },

    // Removes entrance from the object.
    deleteCookieKey( state, cookie = {} )
    {
        const { name, key, days } = addDefaultOptions( cookie )

        ifExists( name, state, () =>
        {
            Vue.delete( state[ name ], key )
            setCookie( window.document, { name, value: state[ name ], days, type: JSON } )
        } )
    },

    // Unshifts to array.

    // Pops from array.

    // Completely overwrites with null.
    deleteCookie( state, cookie = {} )
    {
        const { name, days } = addDefaultOptions( cookie )
        setCookie( window.document, { name, value: null, days, type: STRING } )
    }
}

export const actions =
{
    initCookies( { commit }, req )
    {
        commit( "initCookies", req )
    },
    setCookie( { commit }, cookie )
    {
        commit( "setCookie", cookie )
    },
    setCookieKey( { commit }, cookie )
    {
        commit( "setCookieKey", cookie )
    },
    deleteCookieKey( { commit }, cookie )
    {
        commit( "deleteCookieKey", cookie )
    },
    deleteCookie( { commit }, cookie )
    {
        commit( "deleteCookie", cookie )
    }
}
