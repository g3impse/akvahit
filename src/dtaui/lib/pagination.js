export function renderPagination( o = {} )
{
    const { page, pages, link, paginationToUrl } = o

    let length = o.length || 3
    length = length >= pages ? pages - 1 : length

    const sign = page > Math.ceil( pages / 2 ) ? -1 : 1
    const deviation = ( sign === 1 ? page : pages - page ) < Math.floor( length / 2 ) ? ( sign === 1 ? 0 : pages - page ) : Math.floor( length / 2 )
    const _deviation = ( sign === 1 ? ( deviation > page - 1 ? page - 1 : deviation ) : ( deviation > pages - page ? -pages : deviation ) )

    let pagination = [ 1,
        ...Array.from( { length }, ( _, i ) => ( sign ) * ( i + ( sign ) * ( page - sign * _deviation ) ) ), pages
    ]
        .sort( ( a, b ) => a - b )
        .filter( ( x, i, self ) => self.indexOf( x ) === i )

    pagination = pagination.map( ( x ) =>
    {
        return { value: x, link: paginationToUrl( { url: link, page: x } ) }
    } )

    // pagination.map( ( x, i, self ) =>
    // {
    //     if ( self[ i + 1 ] && self[ i + 1 ].value - x.value > 1 )
    //     {
    //         return i + 1
    //     }
    //     else
    //     {
    //         return null
    //     }
    // } )
    //     .filter( Boolean )
    //     .sort( ( a, b ) => b - a )

    return pagination
}
