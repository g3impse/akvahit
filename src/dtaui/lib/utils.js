function tryR( tryBlock, catchBlock )
{
    try
    {
        return tryBlock()
    }
    catch ( e )
    {
        return catchBlock( e )
    }
}

export async function tryAR( tryBlock, catchBlock )
{
    try
    {
        return await tryBlock()
    }
    catch ( e )
    {
        return await catchBlock( e )
    }
}

function r( returnValue )
{
    const r = { ok: false, err: [] }

    if ( returnValue === true )
    {
        r.ok = true
    }
    else if ( returnValue && returnValue.length > 0 )
    {
        r.err = returnValue
    }

    // Throw error if not ok and err is not an array, or an empty array.
    if ( !r.ok && ( !r.err || !Array.isArray( r.err ) || r.err.length === 0 ) )
    {
        throw new Error( "Not ok and err is empty." )
    }

    return r
}

function includesR( { arr, str, msg, throwErr } )
{
    if ( !msg )
    {
        msg = `'${ arr.join( ", " ) }' does not include ${ str }.`
    }

    if ( arr.includes( str ) )
    {
        return r( true )
    }
    else
    {
        if ( throwErr )
        {
            throw new Error( msg )
        }
        else
        {
            return r( [ new Error( msg ) ] )
        }
    }
}

export function formatNumber( num, separator = " " )
{
    if ( num === undefined || num === null )
    {
        console.warn( "[formatNumber] Wrong Number ", num )
        return ""
    }
    const tmp = num.toString().split( "." )
    let value = tmp[ 0 ].replace( /\D/g, "" ).replace( /\B(?=(\d{3})+(?!\d))/g, separator )
    if ( Number( tmp[ 1 ] ) )
    {
        value += `.${tmp[ 1 ]}`
    }
    return value
}

export function getPrice( num )
{
    if ( num === undefined || num === null )
    {
        console.warn( "[getPrice] Wrong Number ", num )
        return "Цена по запросу"
    }
    else if ( num === 0 )
    {
        return "Цена по запросу"
    }

    // Если честно я не знаю имеет ли смысл код Гриши, если учесть, что для этого есть нативный метод
    // https://developer.mozilla.org/ru/docs/Web/JavaScript/Reference/Global_Objects/Number/toLocaleString
    // Он позволяет делать мультиязычное отображение чисел.
    return `${formatNumber( Math.ceil( num ) )} руб`
}

export function includesM( { superset, subset } )
{
    return subset.filter( x => !superset.includes( x ) ).length === 0
}

export function de( o = {} )
{
    const res = Object.values( o ).indexOf( undefined )

    if ( res !== -1 )
    {
        throw new Error( `${ Object.keys( o )[ res ] } is undefined` )
    }
}

export {
    tryR,
    r,
    includesR
}
