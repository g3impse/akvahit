import tippy from "tippy.js"

function init( htmlNode, opts = {} )
{
    let configuration = {
        allowHTML: true
    }
    configuration = Object.assign( configuration, opts )
    return tippy( htmlNode, configuration )
}

export {
    init
}
