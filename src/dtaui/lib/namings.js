export function getNaming( n, casesParts )
{
    if ( !n || !casesParts || casesParts.length === 0 )
    {
        return null
    }
    const mod10 = n % 10
    const mod100 = n % 100
    if ( mod100 === 1 )
    {
        return `${casesParts[ 0 ]}`
    }
    else if ( ( mod10 >= 5 ) || ( mod10 === 0 ) || ( ( mod100 > 10 ) && ( mod100 < 20 ) ) )
    {
        return `${casesParts[ 2 ]}`
    }
    else
    {
        return `${casesParts[ 1 ]}`
    }
}

export function getDeclOfCategory( n )
{
    return getNaming( n, [ "категория", "категории", "категорий" ] )
}

export function getDeclOfProduct( n )
{
    return getNaming( n, [ "товар", "товара", "товаров" ] )
}

export function getDays( n )
{
    return getNaming( n, [ "день", "дня", "дней" ] )
}
