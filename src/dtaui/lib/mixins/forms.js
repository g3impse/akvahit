import { getPolicyUrl, getConditionsUrl } from "../url.js"
import { getRandomId } from "../utilsDom.js"

export const formMixin = {
    data()
    {
        return {
            result       : false,
            modalId      : getRandomId(),
            policyUrl    : getPolicyUrl(),
            conditionsUrl: getConditionsUrl()
        }
    },

    methods: {
        async submit( data )
        {
            delete data.checkbox1
            delete data.checkbox2

            try
            {
                const res = await this.$api.forms.send( this.code, {
                    ...data,
                    ...this.data
                } )

                // Success.
                if ( !res.data.errors || res.data.errors.length === 0 )
                {
                    this.result = "success"
                    if ( this.resultType !== "inline" )
                    {
                        this.$modal.open( this.modalId )
                    }
                    this.cleanValues()
                    this.$emit( "success" )
                }
                // Fail.
                else
                {
                    for ( const item of res.data.errors )
                    {
                        this.errors = { ...this.errors, ...item.data }
                    }
                }
            }
            catch ( e )
            {
                console.log( "Form/Submit: ", e )

                this.result = "error"
                if ( this.resultType !== "inline" )
                {
                    this.$modal.open( this.modalId )
                }
            }
        },

        error( errors )
        {
            this.errors = { ...this.errors, ...errors }
        },

        onChange( e, target )
        {
            this[ target ] = e
            this.errors[ target ] = ""
        }
    }
}
