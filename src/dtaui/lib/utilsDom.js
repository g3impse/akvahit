import { nanoid } from "nanoid"

const tooltipEvents = [ "click", "mouseenter", "focus", "focusin" ]

export {
    tooltipEvents
}

export function lockBody()
{
    document.body.style.paddingRight = `${
        window.innerWidth - document.documentElement.clientWidth
    }px`
    document.body.style.height = "100%"
    document.body.style.overflow = "hidden"
}

export function unlockBody()
{
    document.body.style.height = ""
    document.body.style.overflow = ""
    document.body.style.paddingRight = ""
}

export function getRandomId()
{
    return nanoid()
}
