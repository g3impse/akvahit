import { writeFileSync } from "fs"
import { resolve } from "path"

import tailwind from "../tailwind.config.js"

const TAILWIND_DICT_FILE = resolve( "./conf/tailwind-dict.json" )

function genColorsDict( obj )
{
    const res = {}
    const isObject = val =>
        typeof val === "object" && !Array.isArray( val )

    const addDelimiter = ( a, b ) =>
        a ? `${a}-${b}` : b

    const paths = ( obj = {}, head = "" ) =>
    {
        return Object.entries( obj )
            .reduce( ( product, [ key, value ] ) =>
            {
                const fullPath = addDelimiter( head, key )

                if ( typeof value === "string" )
                {
                    res[ fullPath ] = value
                }

                return isObject( value )
                    ? product.concat( paths( value, fullPath ) )
                    : product.concat( fullPath )
            }, [] )
    }

    paths( obj )

    return res
}

function genTailwindDict()
{
    const theme = {}

    theme.colors = genColorsDict( tailwind.theme.colors )

    writeFileSync( TAILWIND_DICT_FILE, JSON.stringify( theme ) )
}

export {
    genTailwindDict
}
