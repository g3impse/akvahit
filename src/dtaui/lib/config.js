import { resolve } from "path"
import { readFileSync, writeFileSync } from "fs"
import { tryR, includesR } from "./utils.js"

const tailwind = require( resolve( "./tailwind.config.js" ) )

const SERVERS_FILE = "./conf/servers.json"
const PORTS_FILE   = "./conf/ports.json"
const CUSTOM_CONFIG_FILE = "./custom.nuxt.config.js"
const INJECT_SCRIPTS_FILE = "./conf/inject-scripts.js"
const MODULES_FILE = "./conf/modules.json"
const PLUGINS_FILE = "./conf/plugins.json"
const ROUTER_FILE = "./conf/router.json"
const TAILWIND_DICT_FILE = "./conf/tailwind-dict.json"
const URL_CONSTS_FILE = "./conf/url-consts.json"

function genAppHtml()
{
    // Create (modify) app.html.
    // Add counters and scripts from config.
    const injectHead = { start: [], end: [] } // Arrays of direct html.
    const injectBody = { start: [], end: [] }
    if ( process.env.MODE === "production" )
    {
        const scriptsConfig = tryR(
            () => require( resolve( INJECT_SCRIPTS_FILE ) ).default,
            ( e ) => { throw new Error( "[Production mode error] " + e ) }
        )

        scriptsConfig.forEach( ( x ) =>
        {
            if ( x.head )
            {
                injectHead.end.push( x.head )
            }
            if ( x.headStart )
            {
                injectHead.start.push( x.headStart )
            }
            if ( x.headEnd )
            {
                injectHead.end.push( x.headEnd )
            }
            if ( x.body )
            {
                injectBody.start.push( x.body )
            }
            if ( x.bodyStart )
            {
                injectBody.start.push( x.bodyStart )
            }
            if ( x.bodyEnd )
            {
                injectBody.end.push( x.bodyEnd )
            }
        } )
    }
    const appHtml = `
    <!DOCTYPE html>
    <html {{ HTML_ATTRS }}>
        <head {{ HEAD_ATTRS }}>
            ${ injectHead.start.join( "\n" ) }
            {{ HEAD }}
            ${ injectHead.end.join( "\n" ) }
        </head>
        <body {{ BODY_ATTRS }}>
            ${ injectBody.start.join( "\n" ) }
            {{ APP }}
            ${ injectBody.end.join( "\n" ) }
        </body>
    </html>
    `

    writeFileSync( resolve( "app.html" ), appHtml )
}

function configProxy()
{
    const proxy = tryR(
        () => require( resolve( CUSTOM_CONFIG_FILE ) ).default.proxy || {},
        () => { return {} }
    )

    const { portBack, portFront } = tryR(
        () => JSON.parse( readFileSync( resolve( PORTS_FILE ) ) ),
        () => { return { portBack: 8000, portFront: 3000 } }
    )

    if ( process.env.BACK === "local" )
    {
        const server = `http://localhost:${portBack}`
        proxy[ "/media/" ] = server
        proxy[ "/api/" ] = server
    }
    else if ( process.env.BACK === "test-server" )
    {
        const destination = tryR( () => JSON.parse( readFileSync( resolve( SERVERS_FILE ) ) ).destination || `localhost:${ portBack }`, () => `localhost:${ portBack }` )
        const protocol = tryR( () => JSON.parse( readFileSync( resolve( SERVERS_FILE ) ) ).protocol || "http", () => "http" )

        const server = `${ protocol }://${ destination }`

        proxy[ "/media/" ] = server
        proxy[ "/api/" ] = server
    }
    else if ( process.env.BACK === "socket" )
    {
        const server = `http://localhost:${portBack}`
        proxy[ "/media/" ] = server
        proxy[ "/api/" ] = server
        process.env.PORT = portFront
    }

    return proxy
}

// Common function for configuring anything consisting of two separate entities of the same kind,
// i.e. plugins in custom config, and plugins in conf dir.
// Can be either objects or arrays.
function configCommonEntity( o )
{
    const { customConfigFile, mergingConfigJsonFile, objName, type } = o

    includesR(
        {
            throwErr: true,
            str     : type,
            arr     : [ "array", "object" ],
            msg     : "'type' argument should be either 'object' or 'array'."
        } )

    const fallback = type === "array" ? [] : {}

    const customEntity = tryR(
        () => require( resolve( customConfigFile ) ).default[ objName ] || fallback,
        () => { return fallback }
    )

    const commonEntity = tryR(
        () => JSON.parse( readFileSync( resolve( mergingConfigJsonFile ) ) ) || fallback,
        () => { return fallback }
    )

    return type === "array" ? [ ...commonEntity, ...customEntity ] : { ...commonEntity, ...customEntity }
}

function configModules()
{
    return configCommonEntity( { customConfigFile: CUSTOM_CONFIG_FILE, mergingConfigJsonFile: MODULES_FILE, objName: "modules", type: "array" } )
}

function configPlugins()
{
    return configCommonEntity( { customConfigFile: CUSTOM_CONFIG_FILE, mergingConfigJsonFile: PLUGINS_FILE, objName: "plugins", type: "array" } )
}

function configRouter()
{
    return configCommonEntity( { customConfigFile: CUSTOM_CONFIG_FILE, mergingConfigJsonFile: ROUTER_FILE, objName: "router", type: "object" } )
}

function configCustom()
{
    const customConfig = tryR(
        () => require( resolve( CUSTOM_CONFIG_FILE ) ).default,
        () => { return {} }
    )

    delete customConfig.proxy
    delete customConfig.modules
    delete customConfig.plugins
    delete customConfig.router

    return customConfig
}

function genColorsDict( obj )
{
    const res = {}
    const isObject = val =>
        typeof val === "object" && !Array.isArray( val )

    const addDelimiter = ( a, b ) =>
        a ? `${a}-${b}` : b

    const paths = ( obj = {}, head = "" ) =>
    {
        return Object.entries( obj )
            .reduce( ( product, [ key, value ] ) =>
            {
                const fullPath = addDelimiter( head, key )

                if ( typeof value === "string" )
                {
                    res[ fullPath ] = value
                }

                return isObject( value )
                    ? product.concat( paths( value, fullPath ) )
                    : product.concat( fullPath )
            }, [] )
    }

    paths( obj )

    return res
}

function genTailwindDict()
{
    const theme = {}

    theme.colors = genColorsDict( tailwind.theme.colors )

    writeFileSync( TAILWIND_DICT_FILE, JSON.stringify( theme ) )
}

// For lib/url.js
function genUrlConsts()
{
    const allRoutes = require( "../../.nuxt/routes.json" )

    const formatRoutes = ( obj ) =>
    {
        if ( !obj.children )
        {
            return obj
        }
        else
        {
            const res = []
            for ( const x of obj.children )
            {
                res.push( { ...x, path: `${obj.path}${x.path}` } )
            }

            return res.map( e => formatRoutes( e ) ).concat( { path: obj.path, component: obj.component } )
        }
    }

    if ( allRoutes && allRoutes.length )
    {
        let formattedRoutes = []
        for ( const route of allRoutes )
        {
            formattedRoutes = formattedRoutes.concat( formatRoutes( route ) )
        }

        const maskRoutes = formattedRoutes.map( ( e ) =>
        {
            const regex = e.path === "/" ? "\\.ru/$" : `${ e.path.replace( /\/:city|\?|\*/, "" ).replace( /:region|:id|:guid|:slug|:fslug|:categorySlug/g, "[\\s\\S]+" ) }$`

            const separator = e.component.includes( "new" ) ? "new" : "src"
            const path = `.${ e.component.split( separator )[ 1 ] }`
            const page = readFileSync( path, "utf8" )

            const regexConst = /import { [\S ]+ } from "@\/lib\/url\/const"/
            const regexApi = /\$api/
            let code
            const str = page.match( regexConst )

            if ( str && str[ 0 ] )
            {
                code = str[ 0 ].replace( "import { ", "" ).replace( " } from \"@/lib/url/const\"", "" ).split( ", " )[ 0 ].split( " as " )[ 0 ]
            }

            return {
                ...e,
                code,
                hasRequest: regexApi.test( page ),
                mask      : regex
            }
        } )

        writeFileSync( URL_CONSTS_FILE, JSON.stringify( maskRoutes ) )
    }
}

export {
    genAppHtml,
    configProxy,
    configCustom,
    configModules,
    configPlugins,
    configRouter,
    genTailwindDict,
    genUrlConsts
}
