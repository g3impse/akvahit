export function getFullUrl()
{
    let url
    if ( !process.server )
    {
        url = window.location.href
    }
    return url
}

export function getPolicyUrl()
{
    return "/politika-konfidentsialnosti/"
}

export function getConditionsUrl()
{
    return "/polzovatelskoe-soglashenie/"
}

export function paginationToUrl( o = {} )
{
    const { url, page } = o
    return `${url}page/${page}/`
}

export function urlToPagination( o = {} )
{
    const { url } = o

    if ( !( url && url.length > 0 ) )
    {
        throw new Error( `Нет корректного url, ${url}` )
    }

    const _tmp = url.split( "page/" )
    const result = { url: _tmp[ 0 ], page: _tmp[ 1 ].match( /\d+/ )[ 0 ] }
    return result
}
