const theme = require( "@/conf/tailwind-dict.json" )

export default ( { App }, inject ) =>
{
    inject( "theme", theme )
}
