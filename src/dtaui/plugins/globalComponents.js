import Vue from "vue"

import BaseAccordion from "~/dtaui/components/BaseAccordion"
import BaseButton from "~/dtaui/components/BaseButton"
import BaseForm from "~/dtaui/components/BaseForm"
import BaseImage from "~/dtaui/components/BaseImage"
import BaseList from "~/dtaui/components/BaseList"
import BaseModal from "~/dtaui/components/BaseModal"
import BasePagination from "~/dtaui/components/BasePagination"
import BaseSelect from "~/dtaui/components/BaseSelect"
import BaseShowmore from "~/dtaui/components/BaseShowmore"
import BaseSwitcher from "~/dtaui/components/BaseSwitcher"
import BaseTooltip from "~/dtaui/components/BaseTooltip"
import BaseT from "~/dtaui/components/BaseT"
import IntersectionObserver from "~/dtaui/components/IntersectionObserver"
import ListItem from "~/dtaui/components/ListItem"
import BaseScrollbox from "~/dtaui/components/BaseScrollbox"
import BaseTabs from "~/dtaui/components/BaseTabs"
import BaseRadioButton from "~/dtaui/components/BaseRadioButton"
import BaseCounter from "~/dtaui/components/BaseCounter"
import BaseInput from "~/dtaui/components/BaseInput"

import Clickoutside from "~/dtaui/lib/directives/clickoutside.js"

const components =
{
    BaseAccordion,
    BaseButton,
    BaseForm,
    BaseImage,
    BaseList,
    BaseModal,
    BasePagination,
    BaseSelect,
    BaseShowmore,
    BaseSwitcher,
    BaseTooltip,
    BaseT,
    IntersectionObserver,
    ListItem,
    BaseScrollbox,
    BaseTabs,
    BaseRadioButton,
    BaseCounter,
    BaseInput
}

const directives = {
    ...BaseForm.directives,
    clickoutside: Clickoutside
}

Object.entries( directives ).forEach( ( [ name, directive ] ) =>
{
    Vue.directive( name, directive )
} )
Object.entries( components ).forEach( ( [ name, component ] ) =>
{
    Vue.component( name, component )
} )
