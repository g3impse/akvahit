import Vue from "vue"

// TODO: Generate portal-target when adding plugin to the project.
const ModalPlugin = {
    install( Vue, options = {} )
    {
        Vue.prototype.$modal = {
            event: new Vue(),

            open( id )
            {
                this.event.$emit( "open", id )
            },

            close( id )
            {
                this.event.$emit( "close", id )
            }
        }
    }
}

Vue.use( ModalPlugin )
