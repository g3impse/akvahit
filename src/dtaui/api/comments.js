export default ( { get, post } ) =>
    ( {
        list( model, params )
        {
            return get( `/api/comments/${ model }/list`, params )
        },

        listAll( model, params )
        {
            return get( `/api/comments/${ model }/list`, params, { all: true } )
        },

        create( model, params )
        {
            return post( `/api/comments/${ model }/create`, params )
        }
    } )
