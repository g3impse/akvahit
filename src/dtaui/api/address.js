export default ( { get } ) =>
    ( {
        get( params )
        {
            return get( "/api/dadata", params )
        }
    } )
