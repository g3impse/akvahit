export default ( { get } ) =>
    ( {
        async getId( { fslug } )
        {
            if ( fslug === undefined )
            {
                console.warn( "No fslug passed." )
                return null
            }

            return parseInt( ( await get( "/api/shop/brand", { fslug } ) ).id )
        },

        async get( params )
        {
            let { id } = params
            const { fslug } = params

            if ( id === undefined )
            {
                id = await this.getId( { fslug } )
            }

            if ( id !== undefined )
            {
                return get( `/api/shop/brand/${ id }`, {} )
            }
            else
            {
                console.warn( "No ID for the brand.", params )
                return {}
            }
        }
    } )
