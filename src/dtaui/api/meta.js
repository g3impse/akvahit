export default ( { get } ) =>
    ( {
        get( params = {} )
        {
            return get( "/api/meta/get", params )
        },

        relink( params = {} )
        {
            return get( "/api/meta/relink", params )
        }
    } )
