export default ( { get } ) =>
    ( {
        get( params )
        {
            return get( "/api/time/epoch", params )
        }
    } )
