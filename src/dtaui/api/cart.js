export default ( { get, post } ) =>
    ( {
        get( params )
        {
            return get( "/api/basket", params )
        },

        set( data )
        {
            return post( "/api/basket/set", data )
        },

        add( data )
        {
            return post( "/api/basket/add", data )
        },

        remove( data )
        {
            return post( "/api/basket/remove", data )
        }
    } )
