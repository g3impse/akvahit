export default ( { get, post } ) =>
    ( {
        get( params )
        {
            return get( "/api/delivery/calculator", params )
        }
    } )
