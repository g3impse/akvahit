export default ( { get } ) =>
    ( {
        relation( params = {} )
        {
            return get( "/api/core/relation", params )
        }
    } )
