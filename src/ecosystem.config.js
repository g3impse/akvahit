module.exports = {
    apps: [
        {
            name: "WebsiteAkvahit",
            exec_mode: "cluster",
            instances: "5", // Or a number of instances
            script: "./node_modules/nuxt/bin/nuxt.js",
            env:
            {
                BACK: "socket",
                MODE: "production"
            },
            args: "start"
        }
    ]
}
