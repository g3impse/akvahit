import { resolve } from "path"
import * as configFunctions from "./lib/config.js"

configFunctions.genAppHtml()
configFunctions.genTailwindDict()

export default
{
    proxy: configFunctions.configProxy(),

    plugins: configFunctions.configPlugins(),

    modules: configFunctions.configModules(),

    router: configFunctions.configRouter(),

    ...configFunctions.configCustom(),

    components: true,

    buildModules:
    [
        "@nuxtjs/eslint-module",
        "@nuxtjs/tailwindcss",
        "@aceforth/nuxt-optimized-images"
    ],

    eslint:
    {
        fix: true
    },

    alias:
    {
        img  : resolve( __dirname, "./assets/img" ),
        style: resolve( __dirname, "./assets/styles" )
    },

    axios:
    {
        proxy: true
    },

    optimizedImages:
    {
        optimizeImages     : true,
        optimizeImagesInDev: false,
        mozjpeg            :
        {
            quality: 80
        },
        optipng:
        {
            optimizationLevel: 3
        },
        pngquant: false,
        gifsicle:
        {
            interlaced       : true,
            optimizationLevel: 3
        },
        webp:
        {
            preset : "default",
            quality: 75
        }
    },

    hooks: {
        build: {
            done()
            {
                configFunctions.genUrlConsts()
            }
        }
    }
}
