import { formatNumber } from "@/lib/utils"

export function getChosenFilters( payload )
{
    const { values, primary, props, seo } = payload
    const result = []

    const genRangeString = ( arr, prop, key, id ) =>
    {
        if ( arr[ 0 ] || arr[ 1 ] )
        {
            const fromValue = Math.floor( arr[ 0 ] ) === Math.floor( prop.min ) ? "" : `от ${ formatNumber( arr[ 0 ] ) }`
            const toValue = Math.ceil( arr[ 1 ] ) === Math.ceil( prop.max ) ? "" : `до ${ formatNumber( arr[ 1 ] ) }`

            if ( fromValue || toValue )
            {
                const str = [ fromValue, toValue ].filter( e => Boolean( e ) ).join( " " )
                result.push( {
                    name  : str,
                    type  : key,
                    propId: id
                } )
            }
        }
    }

    const genCheckboxesString = ( arr, prop, key, id ) =>
    {
        for ( const item of arr )
        {
            const x = prop.props.find( e => e.id === +item )
            if ( x )
            {
                let res = {
                    ...x,
                    type  : key,
                    propId: id
                }

                if ( seo && seo.length )
                {
                    const seoItem = seo.find( e => e.valId === x.id )
                    if ( seoItem )
                    {
                        res = {
                            ...res,
                            ...seoItem
                        }
                    }
                }

                result.push( res )
            }
        }
    }

    for ( const [ key, val ] of Object.entries( values ) )
    {
        const isPrimary = Boolean( primary[ key ] )

        if ( isPrimary )
        {
            const prop = primary[ key ]

            if ( prop && prop.min !== undefined && prop.max !== undefined )
            {
                genRangeString( val, prop, key )
            }
            else if ( prop && prop.props )
            {
                genCheckboxesString( val, prop, key )
            }
        }

        else
        {
            for ( const [ key2, val2 ] of Object.entries( val ) )
            {
                const prop = props.find( e => e.id === +key2 )
                // const propSlug = prop.type

                if ( prop && prop.type === "range" )
                {
                    genRangeString( val2, prop, prop.type, prop.id )
                }
                else if ( prop && prop.type === "checkboxes" )
                {
                    genCheckboxesString( val2, prop, prop.type, prop.id )
                }
            }
        }
    }

    if ( values.has_discount )
    {
        result.push( { name: "Со скидкой", type: "has_discount" } )
    }

    if ( values.in_stock )
    {
        result.push( { name: "В наличии", type: "in_stock" } )
    }

    if ( values.back_order )
    {
        result.push( { name: "Под заказ", type: "back_order" } )
    }

    return result.filter( e => Boolean( e ) )
}
