import { mapState } from "vuex"
import { getConst } from "@/lib/url"
import server from "@/conf/servers.json"

export const meta = {
    async fetch()
    {
        const code = getConst( `${ server.destination }${ this.$route.fullPath }` )
        const id = this.product?.id || this.category?.id || this.brand?.id || this.post?.id || this.author?.id || this.regionId

        const data = { code }

        if ( id )
        {
            data.id = id
        }

        this.serverMeta = await this.$api.meta.get( data )
    },

    head()
    {
        const title = this.serverMeta.title || this.meta?.title
        const meta = [
            {
                hid    : "description",
                name   : "description",
                content: this.serverMeta.description || this.meta?.description
            }
        ]
        const link = []

        if ( this.index )
        {
            meta.push( {
                hid    : "robots",
                name   : "robots",
                content: this.index
            } )
        }

        if ( this.canonical )
        {
            link.push( this.canonical )
        }

        return {
            title,
            meta,
            link
        }
    },

    data()
    {
        return {
            serverMeta: {}
        }
    },

    watch:
    {
        region()
        {
            this.$fetch()
        }
    },

    computed: {
        ...mapState( "regions", [
            "region"
        ] )
    }
}
