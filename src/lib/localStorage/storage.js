import { JSON } from "./const"

// If cookies were inited or not.
// Do not use this name for actuall cookie.
// NOTE: Moved to store directly.
// export const init = null

// Cookies. Each cookie represents both cookie and store.cookie entrance.
// Value is cookie type.
export const city = JSON
export const favorite = JSON
export const compare = JSON
export const cart = JSON
export const customer = JSON
