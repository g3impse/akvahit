import { JSON as COOKIE_JSON, STRING, INTEGER, BOOLEAN, DEFAULT_OPTIONS } from "./cookie/const"
import cookieNames from "./cookie/names"

export function getCookie( document, cookie )
{
    const { name } = cookie
    const type = cookie.type.type ? cookie.type.type : cookie.type

    let defaultValue
    if ( cookie.type.default )
    {
        defaultValue = cookie.type.default
    }

    if ( !name )
    {
        throw new Error( "No cookie name was passed." )
    }

    const value = "; " + document.cookie
    const parts = value.split( "; " + name + "=" )

    if ( parts.length === 2 )
    {
        const raw = parts.pop().split( ";" ).shift()
        switch ( type )
        {
        case COOKIE_JSON:
            return JSON.parse( raw )
        case STRING:
            return raw
        case INTEGER:
            return parseInt( raw )
        case BOOLEAN:
            return Boolean( raw )
        case null:
            return true // For init only. TODO: REWORK.
        default:
            throw new Error( `Unknown cookie type '${ type }'.` )
        }
    }
    else
    {
        switch ( type )
        {
        case COOKIE_JSON:
            return defaultValue || {}
        case STRING:
            return defaultValue || ""
        case INTEGER:
            return defaultValue || 0
        case BOOLEAN:
            return defaultValue || false
        case null:
            return true // For init only. TODO: REWORK.
        default:
            throw new Error( `Unknown cookie type '${ type }'.` )
        }
    }
}

export function setCookie( document, cookie )
{
    const { name, value, type, days } = cookie
    let expires = ""

    if ( !name )
    {
        throw new Error( "No cookie name was passed." )
    }

    if ( days )
    {
        const date = new Date()
        date.setTime( date.getTime() + ( days * 24 * 60 * 60 * 1000 ) )
        expires = "; expires=" + date.toUTCString()
    }

    let raw

    switch ( type )
    {
    case COOKIE_JSON:
        raw = name + "=" + JSON.stringify( value ) + expires + "; path=/"
        break
    case STRING:
        raw = name + "=" + value + expires + "; path=/"
        break
    case INTEGER:
        raw = name + "=" + value + expires + "; path=/"
        break
    case BOOLEAN:
        raw = name + "=" + value + expires + "; path=/"
        break
    default:
        throw new Error( "Unknown cookie type." )
    }

    document.cookie = raw

    return true
}

// Discarded together with the populateStore plugin.
// export function initStore( store )
// {
//     return new Promise( ( resolve, reject ) =>
//     {
//         if ( !store.state.cookie.init )
//         {
//             store.commit( "cookie/initCookies" )
//         }
//         resolve()
//     } )
// }

export function getName( name )
{
    if ( name in cookieNames )
    {
        return cookieNames[ name ]
    }
    else
    {
        throw new Error( "No such cookie." )
    }
}

export function ifExists( name, state, callback )
{
    if ( name in state )
    {
        callback()
    }
    else
    {
        throw new Error( "No such cookie." )
    }
}

export function initState( cookies )
{
    return Object.assign( {}, cookies )
}

export function addDefaultOptions( cookie, defaultOptions )
{
    cookie = Object.assign( {}, cookie )
    defaultOptions = defaultOptions || DEFAULT_OPTIONS

    Object.keys( defaultOptions ).forEach( ( key ) =>
    {
        if ( !( key in cookie ) )
        {
            cookie[ key ] = defaultOptions[ key ]
        }
    } )

    return cookie
}
