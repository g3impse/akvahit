// constants.
export const POSTS = "posts"
export const CATEGORIES = "categories"
export const AUTHORS = "authors"
export const BLOCKS = "block-types"
export const ITEMS = "item-types"
export const IMAGES = "images"
export const FILES = "files"
