/* eslint-disable import/export */

import qs from "qs"
import urlConsts from "../conf/url-consts.json"
import { de } from "../dtaui/lib/utils"

// constants.
import {
    ABOUT,
    AGREEMENT,
    ARTICLE,
    ARTICLES_CATEGORY,
    ARTICLES_CATEGORIES,
    AUTHOR,
    AUTHORS,
    BRAND,
    BRANDS,
    CART,
    CHECKOUT,
    COMPARE,
    DELIVERY,
    FAVORITES,
    FILTER,
    GUARANTEE,
    PAYMENT,
    PAGINATION_PATH,
    PRIVACY,
    PRODUCT,
    PRODUCT_CATEGORY,
    PRODUCT_CHARACTERISTICS,
    PRODUCT_REVIEWS,
    PRODUCTS_MAP,
    REGION,
    REGIONS,
    SERVICE,
    SERVICES
} from "./url/const.js"

export * from "../dtaui/lib/url.js"

export function getPathName()
{
    let url
    if ( !process.server )
    {
        url = window.location.pathname
    }
    return url
}

export function getUrl( constant, o = {} )
{
    const { fslug, city } = o

    switch ( constant )
    {
    case AGREEMENT:
        return getPath( Object.keys( { AGREEMENT } )[ 0 ] )
    case ARTICLE:
        de( { fslug } )
        return `/${ ARTICLES_CATEGORIES.path }${ fslug }/`
    case ARTICLES_CATEGORY:
        de( { fslug } )
        return `/${ ARTICLES_CATEGORY.path }${ fslug }/`
    case ARTICLES_CATEGORIES:
        return getPath( Object.keys( { ARTICLES_CATEGORIES } )[ 0 ] )
    case AUTHOR:
        de( { fslug } )
        return `/${ AUTHOR.path }${ fslug }/`
    case BRAND:
        de( { fslug } )
        return `/${ BRAND.path }${ fslug }/`
    case BRANDS:
        return getPath( Object.keys( { BRANDS } )[ 0 ] )
    case CART:
        return getPath( Object.keys( { CART } )[ 0 ] )
    case DELIVERY:
        return getPath( Object.keys( { DELIVERY } )[ 0 ] )
    case GUARANTEE:
        return getPath( Object.keys( { GUARANTEE } )[ 0 ] )
    case PAYMENT:
        return getPath( Object.keys( { PAYMENT } )[ 0 ] )
    case PRIVACY:
        return getPath( Object.keys( { PRIVACY } )[ 0 ] )
    case PRODUCT:
        de( { fslug } )
        return city ? `/${city}/${ PRODUCT.path }${ fslug }/` : `/${ PRODUCT.path }${ fslug }/`
    case PRODUCT_CATEGORY:
        de( { fslug } )
        return city ? `/${city}/${ PRODUCT_CATEGORY.path }${ fslug }/` : `/${ PRODUCT_CATEGORY.path }${ fslug }/`
    case PRODUCT_CHARACTERISTICS:
        de( { fslug } )
        return `/${ PRODUCT.path }${ fslug }/${ PRODUCT_CHARACTERISTICS.path }`
    case PRODUCT_REVIEWS:
        de( { fslug } )
        return `/${ PRODUCT.path }${ fslug }/${ PRODUCT_REVIEWS.path }`
    case REGION:
        de( { fslug } )
        return `/${REGIONS.path}${ fslug }/`
    case SERVICE:
        de( { fslug } )
        return `/${ SERVICES.path }${ fslug }/`
    case SERVICES:
        return getPath( Object.keys( { SERVICES } )[ 0 ] )
    default:
        throw new Error( "Unknown constant." )
    }
}

export function isUrl( constant, path )
{
    switch ( constant )
    {
    case PRODUCT:
        return path.includes( PRODUCT.path ) && !path.includes( PRODUCT_CHARACTERISTICS.path ) && !path.includes( PRODUCT_REVIEWS.path )
    case PRODUCT_CHARACTERISTICS:
        return path.includes( PRODUCT.path ) && path.includes( PRODUCT_CHARACTERISTICS.path )
    case PRODUCT_REVIEWS:
        return path.includes( PRODUCT.path ) && path.includes( PRODUCT_REVIEWS.path )
    case FILTER:
        return path.includes( FILTER.path ) && !path.endsWith( FILTER.path )
    default:
        throw new Error( "Unknown constant." )
    }
}

export function getMenuProductCategoryUrl( o = {} )
{
    const { link, city } = o
    return city ? `/${city}${ link }` : link
}

// Pagination.
export function paginationToUrl( o = {} )
{
    const { url, page } = o

    if ( page > 1 )
    {
        const ind = url.indexOf( "?" )
        if ( ind > -1 )
        {
            return `${url.slice( 0, ind )}${ PAGINATION_PATH }${ page }/${url.slice( ind )}`
        }

        return `${ url }${ PAGINATION_PATH }${ page }/`
    }
    else
    {
        return url
    }
}

export function urlToPagination( url )
{
    const queryInd = url.indexOf( "?" )
    const queryString = queryInd > -1 ? url.slice( queryInd ) : ""

    const _tmp = url.split( PAGINATION_PATH )
    const urlRes = _tmp[ 0 ].replace( queryString, "" )
    const page = _tmp.length > 1 ? parseInt( _tmp[ 1 ].match( /\d+/ )[ 0 ] ) : 1

    const result = {
        url    : urlRes,
        fullUrl: `${ urlRes }${ queryString }`,
        page
    }

    return result
}

// Filter
export function urlToFslug( url, city )
{
    if ( url.includes( `/${ PRODUCT_CATEGORY.path }` ) )
    {
        const tmp = url
            .replace( `/${city}`, "" )
            .replace( new RegExp( `${ PAGINATION_PATH }.*$` ), "" )
            .replace( new RegExp( `${ FILTER.path }.*$` ), "" )
        return tmp.replace( /^\//, "" ).replace( /\/$/, "" )
    }
    else if ( url.includes( `/${ PRODUCT.path }` ) )
    {
        const tmp = url.replace( new RegExp( `^.*/${ PRODUCT.path }` ), "/" )
            .replace( new RegExp( `${ PRODUCT_CHARACTERISTICS.path }.*$` ), "" )
        return tmp.replace( /^\//, "" ).replace( /\/$/, "" )
    }
    else
    {
        return undefined
    }
}

export function urlToFilter( payload )
{
    const { url, primary, props } = payload
    const separator = "-"

    const result = {
        range     : {},
        checkboxes: {}
    }

    const filterSeparator = FILTER.path || "filter/"
    const filter = url.substring( url.indexOf( filterSeparator ) ).replace( filterSeparator, "" )

    for ( const item of filter.split( "/" ) )
    {
        const arr = item.split( separator )

        const prop = primary[ arr[ 0 ] ] || props.find( e => e.slug === arr[ 0 ] )

        const isPrimary = Boolean( primary[ arr[ 0 ] ] )

        if ( prop && ( prop.type === "range" || ( prop.min !== undefined && prop.max !== undefined ) ) )
        {
            const values = arr.slice( 1 )
            const fromIndex = values.indexOf( "from" )
            const toIndex = values.indexOf( "to" )

            let fromValue = fromIndex > -1 ? parseInt( values[ fromIndex + 1 ] ) : prop.min
            let toValue = toIndex > -1 ? parseInt( values[ toIndex + 1 ] ) : prop.max

            fromValue = ( fromValue < prop.min || fromValue > prop.max ) ? prop.min : fromValue
            toValue = ( toValue > prop.max || toValue < prop.min ) ? prop.max : toValue

            if ( !( fromValue === prop.min && toValue === prop.max ) && ![ fromValue, toValue ].some( isNaN ) )
            {
                isPrimary ? result[ arr[ 0 ] ] = [ fromValue, toValue ] : result.range[ prop.id ] = [ fromValue, toValue ]
            }
        }
        else if ( prop && ( prop.type === "checkboxes" || ( isPrimary && prop.props ) ) )
        {
            const values = arr.slice( 1 ).filter( e => e !== "or" )
            const ids = []

            for ( const item of values )
            {
                const x = prop.props.find( ( e ) =>
                {
                    const slug = e.slug || e.fslug
                    return slug.toLowerCase() === item
                } )

                if ( x )
                {
                    ids.push( x.id )
                }
            }

            isPrimary ? result[ arr[ 0 ] ] = ids : result.checkboxes[ prop.id ] = ids
        }
    }

    return {
        url   : url.replace( `${filterSeparator}${filter}`, "" ),
        values: result
    }
}

export function filterToUrl( payload )
{
    const { url, primary, props, values, query } = payload
    const separator = "-"
    const filterSeparator = FILTER.path || "filter/"

    const result = []

    const genRangeString = ( arr, prop, slug ) =>
    {
        if ( arr[ 0 ] || arr[ 1 ] )
        {
            const fromValue = arr[ 0 ] === Math.floor( prop.min ) ? "" : `from${separator}${arr[ 0 ]}`
            const toValue = arr[ 1 ] === Math.ceil( prop.max ) ? "" : `to${separator}${arr[ 1 ]}`

            if ( fromValue || toValue )
            {
                return [ slug, fromValue, toValue ].filter( e => Boolean( e ) ).join( separator )
            }
        }
    }

    const genCheckboxesString = ( arr, prop, slug ) =>
    {
        const slugs = []

        for ( const item of arr )
        {
            const x = prop.props.find( e => e.id === +item )
            if ( x )
            {
                slugs.push( x.slug || x.fslug )
            }
        }

        if ( slugs.length )
        {
            return `${slug}${separator}${slugs.join( `${separator}or${separator}` )}`
        }
    }

    for ( const [ key, val ] of Object.entries( values ) )
    {
        const isPrimary = Boolean( primary[ key ] )

        if ( isPrimary )
        {
            const prop = primary[ key ]

            if ( prop && prop.min !== undefined && prop.max !== undefined )
            {
                if ( val !== [] )
                {
                    const str = genRangeString( val, prop, key )
                    result.push( str )
                }
            }
            else if ( prop && prop.props )
            {
                const str = genCheckboxesString( val, prop, key )
                result.push( str )
            }
        }

        else if ( typeof val === "object" )
        {
            for ( const [ key2, val2 ] of Object.entries( val ) )
            {
                const prop = props.find( e => e.id === +key2 )
                const propSlug = prop.slug

                if ( prop && prop.type === "range" )
                {
                    const str = genRangeString( val2, prop, propSlug )
                    result.push( str )
                }
                else if ( prop && prop.type === "checkboxes" )
                {
                    const str = genCheckboxesString( val2, prop, propSlug )
                    result.push( str )
                }
            }
        }
    }

    const resultStr = result.filter( e => Boolean( e ) ).join( "/" )

    const queryString = qs.stringify( query, { arrayFormat: "comma", encode: false, skipNulls: true } )

    return `${ url }${ url.slice( -1 ) === "/" ? "" : "/" }${ resultStr.length ? filterSeparator + resultStr + "/" : "" }${ queryString.length ? "?" + queryString : "" }`
}

export function queryToObject( queryValues )
{
    const values = {}

    if ( typeof queryValues !== "object" )
    {
        console.warn( "queryValues must be an object" )
        return
    }
    if ( !Object.keys( queryValues ).length )
    {
        return values
    }

    for ( const name in queryValues )
    {
        if ( Array.isArray( values[ name ] ) )
        {
            if ( Array.isArray( queryValues[ name ] ) )
            {
                values[ name ] = queryValues[ name ].map( ( i ) =>
                {
                    if ( i === "true" )
                    {
                        return true
                    }
                    else if ( i === "false" )
                    {
                        return false
                    }
                    else { return i }
                } )
            }
            else
            {
                values[ name ] = [ queryValues[ name ] === "true" ? true : queryValues[ name ] === "false" ? false : queryValues[ name ] ]
            }
        }
        else
        if ( Array.isArray( queryValues[ name ] ) )
        {
            values[ name ] = queryValues[ name ][ 0 ] === "true" ? true : queryValues[ name ][ 0 ] === "false" ? false : queryValues[ name ][ 0 ]
        }
        else
        {
            values[ name ] = queryValues[ name ] === "true" ? true : queryValues[ name ] === "false" ? false : queryValues[ name ]
        }
    }

    return values
}

export function urlToCity( url )
{
    const tmp = url.match( /\/.*?\//, "" )
    const city = tmp && tmp[ 0 ] ? tmp[ 0 ].replace( /\//g, "" ) : null

    if ( [ PRODUCT.path, PRODUCT_CATEGORY.path ].some( s => s.includes( city ) ) )
    {
        return false
    }
    else
    {
        return city
    }
}

export function getConst( url )
{
    urlConsts.push( {
        mask: `.ru/[\\w-]+/${ FILTER.path }[\\s\\S]+$`,
        code: "FILTER"
    } )

    const route = urlConsts.find( ( e ) =>
    {
        const regex = e.name === "all" ? /.ru\/[\w-]+\/$/ : new RegExp( e.mask ) // На аквахите категория может быть только на 1-м уровне вложенности.
        return regex.test( url.split( "?" )[ 0 ] )
    } )

    if ( route && route.code )
    {
        return route.code
    }
}

export function getPath( constant )
{
    const route = urlConsts.find( e => e.code === constant )

    return route && route.path ? route.path : ""
}

export function getUrlMask( page, options = {} )
{
    switch ( page )
    {
    case ARTICLE:
        return `/${ARTICLES_CATEGORIES.path}{slug}/`
    case ARTICLES_CATEGORY:
        return `/${ARTICLES_CATEGORY.path}{slug}/`
    case ARTICLES_CATEGORIES:
        return `/${ARTICLES_CATEGORIES.path}`
    case AUTHOR:
        return `/${AUTHOR.path}{slug}/`
    case AUTHORS:
        return `/${AUTHOR.path}`
    case BRAND:
        return `/${BRAND.path}{slug}/`
    case BRANDS:
        return `/${BRAND.path}`
    case PRODUCT:
        return `/${PRODUCT.path}{fslug}/`
    case PRODUCT_CATEGORY:
        return "/{fslug}/"
    case PRODUCT_CHARACTERISTICS:
        return `/${PRODUCT.path}{fslug}/${PRODUCT_CHARACTERISTICS.path}`
    case PRODUCT_REVIEWS:
        return `/${PRODUCT.path}{fslug}/${PRODUCT_REVIEWS.path}`
    case REGION:
        return `/${REGIONS.path}{region}/`
    case REGIONS:
        return `/${REGIONS.path}`
    case SERVICE:
        return `/${SERVICES.path}{slug}/`
    case SERVICES:
        return `/${SERVICES.path}`
    default:
        return ""
    }
}

export function isFastLoad( constant )
{
    const page = urlConsts.find( e => e.code === constant )

    switch ( constant )
    {
    case Object.keys( { ABOUT } )[ 0 ]:
        return true
    case Object.keys( { BRANDS } )[ 0 ]:
        return true
    case Object.keys( { CART } )[ 0 ]:
        return true
    case Object.keys( { CHECKOUT } )[ 0 ]:
        return true
    case Object.keys( { COMPARE } )[ 0 ]:
        return true
    case Object.keys( { FAVORITES } )[ 0 ]:
        return true
    case Object.keys( { PRODUCTS_MAP } )[ 0 ]:
        return true
    default:
        return page ? !page.hasRequest : false
    }
}
