// Cookie types.
export const JSON = {}
export const STRING = {}
export const INTEGER = {}
export const BOOLEAN = {}

// Options.
export const DEFAULT_OPTIONS = {
    days: 3600,
    type: JSON
}
