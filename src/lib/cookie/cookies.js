import { JSON, STRING, INTEGER, BOOLEAN } from "./const"

// If cookies were inited or not.
// Do not use this name for actuall cookie.
// NOTE: Moved to store directly.
// export const init = null

// Cookies. Each cookie represents both cookie and store.cookie entrance.
// Value is cookie type.
export const favorite = JSON
export const compare = JSON
export const viewedProducts =  { type: JSON, default: [] }
export const limit = { type: INTEGER, default: 40 }
export const sorting = { type: STRING, default: "default" }
export const view = { type: STRING, default: "grid" }
export const acceptedCookie = BOOLEAN
export const regionId = INTEGER
