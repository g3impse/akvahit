import * as COOKIES from "./cookies"

const cookies = Object.assign( {}, COOKIES )
const exeptions = {
    // Write some exeptions here. Or renamings.
}

Object.keys( cookies ).forEach( ( name ) =>
{
    cookies[ name ] = name
} )

Object.keys( exeptions ).forEach( ( name ) =>
{
    cookies[ name ] = exeptions[ name ]
} )

// NOTE: Use getName function from @/lib/cookie.js module to use this module.
export default cookies
