/* eslint-disable import/export */
import { formatNumber } from "~/dtaui/lib/utils"

export * from "../dtaui/lib/utils.js"

export function getPrice( num )
{
    if ( num === undefined || num === null )
    {
        console.warn( "[getPrice] Wrong Number ", num )
        return "Цена по запросу"
    }
    else if ( num === 0 )
    {
        return "Цена по запросу"
    }

    return `${formatNumber( Math.ceil( num ) )} ₽`
}

export function monthByNumber( num, type = "full" )
{
    if ( typeof num !== "number" || isNaN( num ) || num < 0 || num > 11 )
    {
        console.warn( "[monthByNumber] Wrong number ", num )
        return ""
    }

    const months = {
        0: {
            full : "Январь",
            short: "Янв",
            case : "Января"
        },
        1: {
            full : "Февраль",
            short: "Фев",
            case : "Февраля"
        },
        2: {
            full : "Март",
            short: "Мар",
            case : "Марта"
        },
        3: {
            full : "Апрель",
            short: "Апр",
            case : "Апреля"
        },
        4: {
            full : "Май",
            short: "Май",
            case : "Мая"
        },
        5: {
            full : "Июнь",
            short: "Июн",
            case : "Июня"
        },
        6: {
            full : "Июль",
            short: "Июл",
            case : "Июля"
        },
        7: {
            full : "Август",
            short: "Авг",
            case : "Августа"
        },
        8: {
            full : "Сентябрь",
            short: "Сен",
            case : "Сентября"
        },
        9: {
            full : "Октябрь",
            short: "Окт",
            case : "Октября"
        },
        10: {
            full : "Ноябрь",
            short: "Ноя",
            case : "Ноября"
        },
        11: {
            full : "Декабрь",
            short: "Дек",
            case : "Декабря"
        }
    }

    return months[ num ][ type ]
}

export function prettyDate( dateString, type = "case" )
{
    const date = new Date( dateString )
    return `${date.getDate()} ${monthByNumber( date.getMonth(), type )} ${date.getFullYear()}`
}

export function sleep( sec )
{
    return new Promise( resolve => setTimeout( resolve, sec * 1000 ) )
}

export function plural( num, postfixes )
{
    if ( !num )
    {
        console.warn( "[plural] Wrong Number ", num )
        return ""
    }

    let n = Math.abs( num )
    n %= 100
    if ( n >= 5 && n <= 20 )
    {
        return postfixes[ 2 ]
    }
    n %= 10
    if ( n === 1 )
    {
        return postfixes[ 0 ]
    }
    if ( n >= 2 && n <= 4 )
    {
        return postfixes[ 1 ]
    }
    return postfixes[ 2 ]
}

export function dateToString( date )
{
    if ( !date || !( date instanceof Date ) )
    {
        console.log( "[dateToString] Wrong date, ", date )
        return ""
    }

    const year = date.getFullYear()
    let month = date.getMonth() + 1
    let day = date.getDate()

    if ( month.toString().length < 2 )
    {
        month = "0" + month
    }
    if ( day.toString().length < 2 )
    {
        day = "0" + day
    }
    return `${day}.${month}.${year}`
}

export function lowerFirst( string )
{
    return `${ string.charAt( 0 ).toLowerCase() }${ string.slice( 1 ) }`
}

export function hasUpperCase( string )
{
    return string.toLowerCase() !== string
}

export function getRandom( list )
{
    return list[ Math.floor( ( Math.random() * list.length ) ) ]
}

export function arrayToObject( arr, key )
{
    const res = {}

    for ( const item of arr )
    {
        if ( item[ key ] && !res[ item[ key ] ] )
        {
            res[ item[ key ] ] = item
        }
    }

    return res
}

export function objectToArray( obj )
{
    const res = []

    for ( const item of Object.values( obj ) )
    {
        res.push( item )
    }

    return res
}
