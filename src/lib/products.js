import { getLocalStorage, setLocalStorage, getName } from "@/lib/localStorage"
import { JSON } from "@/lib/localStorage/const"
import { FAVORITE, COMPARE, CART } from "@/lib/products/const"

function getType( constant )
{
    switch ( constant )
    {
    case FAVORITE:
        return getName( "favorite" )
    case COMPARE:
        return getName( "compare" )
    case CART:
        return getName( "cart" )
    default:
        throw new Error( "Unknown constant." )
    }
}

export async function getProducts( constant, o = {} )
{
    let result = []

    const name = getType( constant )
    const { dict, request } = o // request = $api.products.get

    if ( process.client && isLocalStorageActual( name, dict ) )
    {
        const localStorage = getLocalStorage( { name, type: JSON } )
        result = Object.values( localStorage )
    }
    else
    {
        const ids = Object.keys( dict )

        if ( ids.length )
        {
            const productsRes = await request( { ids } )
            result = productsRes.results
        }
    }

    return result
}

export function addProduct( constant, o = {} )
{
    const name = getType( constant )
    const { product, action } = o

    const localStorage = getLocalStorage( { name, type: JSON } )
    setLocalStorage( { name, value: { ...localStorage, [ product.id ]: product } } )

    action( product )
}

export function removeProduct( constant, o = {} )
{
    const name = getType( constant )
    const { product, products, action } = o

    const localStorage = getLocalStorage( { name, type: JSON } )
    delete localStorage[ product.id ]
    setLocalStorage( { name, value: localStorage } )

    action( product )

    if ( products )
    {
        return products.filter( e => e.id !== product.id )
    }
}

export function clearProducts( constant, o = {} )
{
    const { action } = o

    const name = getType( constant )
    setLocalStorage( { name, value: {} } )

    action()

    return []
}

export function toggleProduct( constant, o = {} )
{
    if ( constant === CART )
    {
        throw new Error( "Incorrect constant." )
    }

    isIn( o ) ? removeProduct( constant, o ) : addProduct( constant, o )
}

export function isIn( o = {} )
{
    const { product, dict } = o
    return Boolean( dict && dict[ product.id ] )
}

function isLocalStorageActual( name, dict )
{
    const array1 = Object.values( getLocalStorage( { name, type: JSON } ) ).map( e => e.id )
    const array2 = Object.values( dict ).map( e => e.id )

    return array1.length === array2.length && array1.every( ( value, index ) => value === array2[ index ] )
}
