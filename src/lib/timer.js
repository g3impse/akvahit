export function setTimer( constant, callback )
{
    clearTimeout( constant.id )
    constant.id = setTimeout( callback, constant.ms )
}

export function clearTimer( constant )
{
    clearTimeout( constant.id )
}
