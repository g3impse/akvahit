import { JSON as COOKIE_JSON, STRING, INTEGER, DEFAULT_OPTIONS } from "./localStorage/const"
import storageNames from "./localStorage/names"

export function getLocalStorage( storage )
{
    if ( process.client )
    {
        const { name } = storage
        const type = storage.type.type ? storage.type.type : storage.type

        let defaultValue
        if ( storage.type.default )
        {
            defaultValue = storage.type.default
        }

        if ( !name )
        {
            throw new Error( "No storage name was passed." )
        }

        const res = localStorage.getItem( name )

        if ( res )
        {
            switch ( type )
            {
            case COOKIE_JSON:
                return JSON.parse( res )
            case STRING:
                return res
            case INTEGER:
                return parseInt( res )
            case null:
                return true // For init only. TODO: REWORK.
            default:
                throw new Error( `Unknown localStorage type '${ type }'.` )
            }
        }
        else
        {
            switch ( type )
            {
            case COOKIE_JSON:
                return defaultValue || {}
            case STRING:
                return defaultValue || ""
            case INTEGER:
                return defaultValue || 0
            case null:
                return true // For init only. TODO: REWORK.
            default:
                throw new Error( `Unknown localStorage type '${ type }'.` )
            }
        }
    }

    else
    {
        throw new Error( "Function getLocalStorage can only be used on client side." )
    }
}

export function setLocalStorage( storage )
{
    if ( process.client )
    {
        const { name, value, type } = addDefaultOptions( storage )

        if ( !name )
        {
            throw new Error( "No localStorage name was passed." )
        }

        ifExists( name, storageNames, () =>
        {
            let res

            switch ( type )
            {
            case COOKIE_JSON:
                res = JSON.stringify( value )
                break
            case STRING:
                res = value
                break
            case INTEGER:
                res = parseInt( value )
                break
            default:
                throw new Error( "Unknown localStorage type." )
            }

            localStorage.setItem( name, res )

            return true
        } )
    }

    else
    {
        throw new Error( "Function setLocalStorage can only be used on client side." )
    }
}

export function getName( name )
{
    if ( name in storageNames )
    {
        return storageNames[ name ]
    }
    else
    {
        throw new Error( "No such localStorage." )
    }
}

export function ifExists( name, state, callback )
{
    if ( name in state )
    {
        callback()
    }
    else
    {
        throw new Error( "No such localStorage." )
    }
}

export function addDefaultOptions( storage, defaultOptions )
{
    storage = Object.assign( {}, storage )
    defaultOptions = defaultOptions || DEFAULT_OPTIONS

    Object.keys( defaultOptions ).forEach( ( key ) =>
    {
        if ( !( key in storage ) )
        {
            storage[ key ] = defaultOptions[ key ]
        }
    } )

    return storage
}
