import {
    INDEX,
    ABOUT,
    AGREEMENT,
    ARTICLES_CATEGORY,
    ARTICLES_CATEGORIES,
    AUTHOR,
    AUTHORS,
    BRAND,
    BRANDS,
    CART,
    CHECKOUT,
    COMPARE,
    CONTACTS,
    DELIVERY,
    FAVORITES,
    FILTER,
    GUARANTEE,
    PAGE_NOT_FOUND,
    PAYMENT,
    PRIVACY,
    PRODUCT,
    PRODUCT_CATEGORY,
    PRODUCT_CHARACTERISTICS,
    PRODUCT_REGION,
    PRODUCT_REVIEWS,
    PRODUCTS_MAP,
    QUALITY_CONTROL,
    REFUND,
    REGION,
    REGIONS,
    SEARCH,
    SERVICES, PRODUCT_CATEGORY_REGION
} from "./url/const"

import { getUrl } from "./url"
import { getPrice, lowerFirst, getRandom } from "./utils"

export function getTitle( page, options = {} )
{
    const { product, h1, title, city, code, pageNumber } = options
    let price

    switch ( page )
    {
    case INDEX:
        return `Продажа и поставка инженерного оборудования для систем отопления и водоснабжения ${ city } - АкваХит`
    case ABOUT:
        return "О компании | АкваХит"
    case AGREEMENT:
        return "Пользовательское Соглашение | АкваХит"
    case ARTICLES_CATEGORIES:
        return "Статьи - ООО \"АкваХит\""
    case AUTHOR:
        return "Авторы | ООО \"АкваХит\""
    case AUTHORS:
        return "Авторы - ООО \"АкваХит\""
    case BRAND:
        return `Купить оборудование ${ title } ${ city }, цена в интернет магазине`
    case BRANDS:
        return "Производители теплосчетчиков и распределителей тепла"
    case CART:
        return "Моя корзина"
    case CHECKOUT:
        return "Оформление заказа"
    case COMPARE:
        return "Сравнение"
    case CONTACTS:
        return "Контакты | АкваХит"
    case DELIVERY:
        return "Способы доставки | АкваХит"
    case FAVORITES:
        return "Избранное"
    case FILTER:
        return `${ h1 } купить, цены ${ city }${ pageNumber && pageNumber > 1 ? ` — страница ${ pageNumber }` : "" }`
    case GUARANTEE:
        return "Гарантия | АкваХит"
    case PAGE_NOT_FOUND:
        return `${ code }. ${ title }`
    case PAYMENT:
        return "Способы оплаты для юридических и физических лиц | АкваХит"
    case PRIVACY:
        return "Политика конфиденциальности | АкваХит"
    case PRODUCT:
        return `${ product.title } купить ${ city } – ${ product.title } цена`
    case PRODUCT_CATEGORY:
        return `${ h1 } купить, цены ${ city }${ pageNumber && pageNumber > 1 ? ` — страница ${ pageNumber }` : "" }`
    case PRODUCT_CHARACTERISTICS:
        return `${ product.title } характеристики, фото ${ product.title } инструкция по применению`
    case PRODUCT_REGION:
        if ( product.data.conjointData && product.data.conjointData.minPrice && product.data.conjointData.maxPrice )
        {
            price = `от ${ Math.ceil( product.data.conjointData.minPrice ) } руб`
        }
        else if ( product.data.price )
        {
            price = `${ Math.ceil( product.data?.price ) } руб`
        }
        else
        {
            price = "по запросу"
        }

        return `Купить ${ lowerFirst( product.title ) } ${ city } по цене ${ price } ${ getRandom( [ "недорого", "дешево", "с доставкой", "в интернет-магазине", "на официальном сайте" ] ) }`
    case PRODUCT_REVIEWS:
        return `${ product.title } отзывы покупателей о товаре`
    case PRODUCTS_MAP:
        return "Карта товаров"
    case QUALITY_CONTROL:
        return "Способы доставки | АкваХит"
    case REFUND:
        return "Возврат товара | АкваХит"
    case REGION:
        return `Трубопроводная арматура, отопление, счетчики ${ city } – АкваХит`
    case REGIONS:
        return "Разделы в регионах"
    case SEARCH:
        return "Поиск товаров"
    case SERVICES:
        return "Услуги - ООО \"АкваХит\""
    default:
        throw new Error( "Unknown constant." )
    }
}

export function getDescription( page, options = {} )
{
    const { h1, product, category, title, city, phone, email, address, code, minPrice, pageNumber } = options
    let price

    if ( product && product.data.conjointData && product.data.conjointData.minPrice )
    {
        price = `от ${ Math.ceil( product.data.conjointData.minPrice ) } руб`
    }
    else if ( product && product.data.price )
    {
        price = `${ Math.ceil( product.data.price ) } руб`
    }
    else
    {
        price = "по запросу"
    }

    switch ( page )
    {
    case INDEX:
        return `АкваХит - продажа и поставка инженерного оборудования для систем отопления и водоснабжения ${ city } по доступным ценам`
    case ABOUT:
        return "Компания АкваХит занимается продажей и установкой теплосчетчиков,распределителей тепла. Выполняет работы по автоматизации и монтажу приборов учета тепла на крупных строительных объектах."
    case AGREEMENT:
        return "Пользовательское Соглашение | АкваХит"
    case ARTICLES_CATEGORIES:
        return "Статьи"
    case AUTHOR:
        return "Авторы | ООО \"АкваХит\""
    case AUTHORS:
        return "Авторы"
    case BRAND:
        return `⭐ ⭐ ⭐ ⭐ ⭐ Купить оборудование ${ title } ${ city }. ✅ ${ getRandom( [ "Недорогое", "Дешевое", "Бюджетное" ] ) } оборудование ${ title } - ${ getRandom( [ "цена", "стоимость" ] ) } ${ minPrice ? `${ Math.ceil( minPrice ) } руб.` : "по запросу" } ‼ в интернет магазине АкваХит ${ city }. Оборудование ${ title } ✅ ${ getRandom( [ "на сайте", "на официальном сайте", "в интернет магазине" ] ) } с доставкой.`
    case BRANDS:
        return "Полная информация о производителях теплосчетчиков и распределителей тепла. Описание, документация."
    case CART:
        return "Моя корзина"
    case CHECKOUT:
        return "Оформление заказа"
    case COMPARE:
        return "Сравнение"
    case CONTACTS:
        return `Телефон: ${ phone } | E-Mail: ${ email } | Адрес: ${ address } |  График работы: по будням с 9:00 до 18:00`
    case DELIVERY:
        return "Компания АкваХит осуществляет доставку по Москве и всей России, также Вы можете забрать товары из пунктов самовывоза"
    case FAVORITES:
        return "Избранное"
    case FILTER:
        return `${ h1 } по привлекательной цене в интернет-магазине АкваХит. Широкий ассортимент качественного оборудования для систем отопления и водоснабжения. Закажите ${ h1 } в интернет-магазине Аквахит.${ pageNumber && pageNumber > 1 ? ` Страница ${ pageNumber }` : "" }`
    case GUARANTEE:
        return "На всё оборудование в нашем интернет-магазине действует гарантия! Срок гарантийного обслуживания колеблется от 12 до 60 месяцев в зависимости от вида оборудования и производителя"
    case PAGE_NOT_FOUND:
        return `${ code }. ${ title }`
    case PAYMENT:
        return "Способы оплаты для юридических и физических лиц | АкваХит"
    case PRIVACY:
        return "Политика конфиденциальности | АкваХит"
    case PRODUCT:
        return `⭐ ⭐ ⭐ ⭐ ⭐ ${ product.title } купить в интернет-магазине. Заказать ✅ ${ product.title } ✅ по цене 💲 ${ price }💲 с доставкой. Широкий ассортимент качественного оборудования для систем отопления и водоснабжения. ‼ ${ product.title } ‼ ${ city } в наличии.`
    case PRODUCT_CATEGORY:
        return `⭐ ⭐ ⭐ ⭐ ⭐ ${ h1 } купить в интернет-магазине. Заказать ✅ ${ h1 } ✅ по цене от 💲 ${ Math.ceil( category.data.minPrice ) } руб. 💲 с доставкой. Широкий ассортимент качественного оборудования для систем отопления и водоснабжения. ‼ ${ h1 } в наличии.${ pageNumber && pageNumber > 1 ? ` Страница ${ pageNumber }` : "" }`
    case PRODUCT_CHARACTERISTICS:
        return `⭐ ⭐ ⭐ ⭐ ⭐ ${ product.title } технические характеристики. Фото ✅ ${ product.title } ✅ и инструкция по применению. Широкий ассортимент качественного оборудования для систем отопления и водоснабжения. ‼ ${ product.title } ‼ ${ city } в наличии.`
    case PRODUCT_REGION:
        return `⭐ ⭐ ⭐ ⭐ ⭐ ${ product.title }, купить ${ city }. ✅ ${ getRandom( [ "Недорогой", "Дешевый", "Бюджетный" ] ) } ${ lowerFirst( product.title ) } - ${ getRandom( [ "цена", "стоимость" ] ) } ${ price } 💲 с доставкой ${ city }. ${ product.title } ✅ ${ getRandom( [ "на сайте", "на официальном сайте", "в интернет магазине" ] ) } АкваХит.`
    case PRODUCT_REVIEWS:
        return `⭐ ⭐ ⭐ ⭐ ⭐ ${ product.title } отзывы покупателей. Широкий ассортимент качественного оборудования для систем отопления и водоснабжения. ‼ ${ product.title } ‼ ${ city } в наличии.`
    case PRODUCTS_MAP:
        return "Карта товаров"
    case QUALITY_CONTROL:
        return "Компания АкваХит осуществляет доставку по Москве и всей России, также Вы можете забрать товары из пунктов самовывоза"
    case REFUND:
        return "Возврат товара | АкваХит"
    case REGION:
        return `Трубопроводная арматура ${ city }. Комплектующие для отопления, запорная арматура, счетчики ${ city } от АкваХит.`
    case REGIONS:
        return "Интернет-магазин \"АкваХит\" в регионах. Трубопроводная арматура, комплектующие для отопления, запорная арматура, счетчики."
    case SEARCH:
        return "Поиск по сайту"
    case SERVICES:
        return "Услуги"
    default:
        throw new Error( "Unknown constant." )
    }
}

export function getH1( page, options = {} )
{
    const { product, category, city, filter, query, title, seo } = options

    const titleFilter = filter && filter.length ? filter.filter( e => ![ "price", "has_discount", "in_stock", "back_order" ].includes( e.type ) ) : []
    const filterBefore = titleFilter && titleFilter.length ? titleFilter.filter( e => e.position === "before" ).sort( ( a, b ) => a.ordering > b.ordering ).map( e => e.title || e.name ).join( ", " ) : ""
    const filterAfter = titleFilter && titleFilter.length ? titleFilter.filter( e => e.position === "after" || !e.position ).sort( ( a, b ) => a.ordering > b.ordering ).map( e => e.title || e.name ).join( ", " ) : ""
    const filterBetween = titleFilter && titleFilter.length ? titleFilter.filter( e => e.position === "between" || !e.position ).sort( ( a, b ) => a.ordering > b.ordering ).map( e => e.title || e.name ).join( ", " ) : ""

    const filterMiddle = seo && seo.filterTemplate ? seo.filterTemplate.replace( "[between]", filterBetween ) : category?.title

    switch ( page )
    {
    case INDEX:
        return ""
    case ABOUT:
        return "О компании"
    case AGREEMENT:
        return "Пользовательское соглашение"
    case ARTICLES_CATEGORIES:
        return "Статьи"
    case ARTICLES_CATEGORY:
        return `Статьи - ${title}`
    case AUTHOR:
        return "Об авторе"
    case AUTHORS:
        return "Авторы"
    case BRAND:
        return `Оборудование ${ title }`
    case BRANDS:
        return "Производители"
    case CART:
        return "Корзина"
    case CHECKOUT:
        return "Оформление заказа"
    case COMPARE:
        return "Сравнение"
    case CONTACTS:
        return "Контакты"
    case DELIVERY:
        return "Доставка"
    case FAVORITES:
        return "Избранное"
    case GUARANTEE:
        return "Наши гарантии"
    case PAYMENT:
        return "Способы оплаты"
    case PRIVACY:
        return "Политика конфиденциальности"
    case PRODUCT:
        return `${ product.title } ${ city }`
    case PRODUCT_CATEGORY:
        return `${ filterBefore } ${ filterBefore ? lowerFirst( filterMiddle ) : filterMiddle } ${ filterAfter } ${ city }`
    case PRODUCT_CHARACTERISTICS:
        return `${ product.title }: технические характеристики и инструкция по применению`
    case PRODUCT_REGION:
        return `${ product.title } ${ city }`
    case PRODUCT_REVIEWS:
        return `${ product.title } ${ city }`
    case PRODUCTS_MAP:
        return "Наши товары"
    case QUALITY_CONTROL:
        return "Контроль качества"
    case REFUND:
        return "Возврат товара"
    case REGION:
        return `Трубопроводная арматура, отопление, счетчики ${ city }`
    case REGIONS:
        return "Интернет-магазин \"АкваХит\" в регионах"
    case SEARCH:
        return `Результаты поиска: "${ query }"`
    case SERVICES:
        return "Услуги"
    default:
        throw new Error( "Unknown constant." )
    }
}

export function getFaq( page, options = {} )
{
    const { product, category, city, citySlug } = options
    let price

    switch ( page )
    {
    case PRODUCT:
        if ( product.data.conjointData && product.data.conjointData.minPrice )
        {
            price = `от ${ Math.ceil( product.data.conjointData.minPrice ) } руб`
        }
        else if ( product.data.price )
        {
            price = `${ Math.ceil( product.data?.price ) } руб`
        }
        else
        {
            price = "по запросу"
        }

        return [
            {
                key    : `Как можно купить ${ lowerFirst( product.title ) } ${ city }?`,
                content: `Купить ${ lowerFirst( product.title ) } ${ city } можно оформив заказ через сайт.`
            },
            {
                key    : `Если нужно купить ${ lowerFirst( product.title ) } ${ city } недорого, есть скидка?`,
                content: `${ product.title } дешево ${ city } найдете только в нашем интернет-магазине.`
            },
            {
                key    : `Сколько будет стоить ${ lowerFirst( product.title ) } с доставкой ${ city } на дом?`,
                content: `${ product.title } по цене ${ price } не включается доставку. Рассчитать доставку ${ city } можно связавшись с нашими менеджерами, либо ознакомится в <a href="/delivery/">разделе доставки</a>.`
            },
            {
                key    : `Как можно оплатить если закажу ${ lowerFirst( product.title ) }?`,
                content: "Оплатить заказ можно любым <a href=\"/payment/\">удобным для вас способом</a>."
            },
            {
                key    : `Сколько гарантия на ${ lowerFirst( product.title ) }?`,
                content: "Со сроками гарантии можно ознакомиться в <a href=\"/garantiya/\">специальном разделе сайта</a>."
            }
        ]
    case PRODUCT_CATEGORY: {
        const cheapestProduct = category.data.minPriceProduct || {}

        return [
            {
                key    : `Хочу недорого ${ lowerFirst( category.title ) } купить, что предложите?`,
                content: `Есть ${ lowerFirst( category.title ) } с ценой ${ city } от ${ getPrice( cheapestProduct.data?.price ) } — это <a href="${ getUrl( PRODUCT, { fslug: cheapestProduct.fslug, city: citySlug } ) }">${ cheapestProduct.title }</a>`
            },
            {
                key    : `Какая стоимость доставки ${ city }?`,
                content: "Рассчитать детали доставки можно связавшись с менеджерами, либо ознакомится в <a href=\"/delivery/\">разделе доставки</a>."
            },
            {
                key    : "Какие способы оплаты заказа есть в вашем магазине?",
                content: "Оплатить заказ можно любым <a href=\"/payment/\">удобным для вас способом</a>."
            }
        ]
    }

    default:
        throw new Error( "Unknown constant." )
    }
}

export function getSeoText( page, options = {} )
{
    const { product, category, brand, city, count, minPrice } = options
    let price

    switch ( page )
    {
    case PRODUCT:
        if ( product.data.conjointData && product.data.conjointData.minPrice && product.data.conjointData.maxPrice )
        {
            price = `от ${ Math.ceil( product.data.conjointData.minPrice ) } до ${ Math.ceil( product.data.conjointData.maxPrice ) } руб`
        }
        else if ( product.data.price )
        {
            price = `${ Math.ceil( product.data?.price ) } руб`
        }
        else
        {
            price = "по запросу"
        }

        return [
            `✅ ${ product.title } купить ${ city } с доставкой`,
            `✅ ${ product.title } цена ${ price }`,
            `✅ Купите ${ lowerFirst( product.title ) } в интернет-магазине Аквахит`
        ]
    case PRODUCT_REGION:
        return [
            `✅ Купить ${ lowerFirst( product.title ) } ${ city }.`,
            `✅ ${ getRandom( [ "Цена", "Стоимость" ] ) } на ${ lowerFirst( product.title ) } с доставкой.`,
            `✅ ${ product.title } ${ getRandom( [ "недорого", "дешево", "бюджетно" ] ) } ${ city }.`
        ]
    case PRODUCT_CATEGORY:
        return [
            `✅ Купить ${ lowerFirst( category.title ) } в интернет-магазине`,
            "✅ Широкий ассортимент оборудования",
            `✅ ${ count } видов ${ lowerFirst( category.titleCases?.pl?.gen || category.title ) } по цене от ${ Math.ceil( category.data.minPrice ) } руб.`,
            `✅ ${ category.titleCases?.sg?.base || category.title } купить недорого с доставкой по РФ`
        ]
    case PRODUCT_CATEGORY_REGION:
        return [
            `✅ Купить ${ lowerFirst( category.title ) } ${ city }.`,
            `✅ ${ category.title } по цене от ${ Math.ceil( category.data.minPrice ) } руб. ${ city }.`,
            `✅ ${ category.title } недорого ${ city }.`
        ]
    case BRAND:
        return [
            `✅ Купить оборудование ${ brand.name } ${ city }.`,
            `✅ Оборудование ${ brand.name } - цена ${ minPrice ? `от ${ Math.ceil( minPrice ) } руб` : "по запросу" }.`,
            `✅ Оборудование ${ brand.name } ${ getRandom( [ "недорого", "дешево", "бюджетно" ] ) } ${ city }.`
        ]
    default:
        throw new Error( "Unknown constant." )
    }
}
