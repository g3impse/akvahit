/* eslint-disable import/export */

export * from "../dtaui/lib/utilsDom.js"

export function removeHeadScripts()
{
    const scripts = document.querySelectorAll( "head script" )
    for ( const script of scripts )
    {
        document.head.removeChild( script )
    }
}
