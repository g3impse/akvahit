import qs from "qs"
import defaultAxios from "axios"
import { tryR } from "@/lib/utils"
import { getCookie } from "@/lib/cookie"
import { STRING } from "@/lib/cookie/const"

export const PARAMS_SERIALIZER = ( params ) => { return qs.stringify( params, { arrayFormat: "comma", encode: true, skipNulls: true } ) }
const DEFAULT_LIMIT = 60

export function init( { axios } )
{
    axios = axios || defaultAxios

    const get = async( method, data = {}, options = {} ) =>
    {
        data.limit = data.limit || DEFAULT_LIMIT
        const get = data => tryR( () => { return axios.get( method, { params: data, paramsSerializer: PARAMS_SERIALIZER } ) }, e => console.warn( "API GET: ", e ) )
        const res = await get( data )

        // Get all pages and merge results.
        if ( options.all )
        {
            const results = []
            // Async following reqs.
            if ( res.data && res.data.next )
            {
                const reqs = Array.from( { length: Math.floor( res.data.count / data.limit ) }, ( x, i ) => i + 2 )
                    .map( x => get( { ...data, page: x } ) )
                const resN  = await Promise.all( reqs );

                [ res, ...resN ].forEach( x => results.push( ...x.data.results ) )
            }
            else if ( res.data && res.data.errors && res.data.errors.length > 0 )
            {
                console.warn( res.data.errors )
            }
            else
            {
                results.push( ...res.data.results )
            }
            return new Promise( ( resolve, reject ) => resolve( results ) )
        }
        else if ( options.result )
        {
            if ( res && res.data && res.data.result )
            {
                return res.data.result
            }
            else
            {
                return {}
            }
        }
        // Straight return results array.
        else if ( options.results )
        {
            if ( res && res.data && res.data.results )
            {
                return res.data.results
            }
            else
            {
                return []
            }
        }
        // Return everything.
        else
        {
            return res.data
        }
    }

    const post = async( method, data = {}, options = {} ) =>
    {
        const res = await tryR( () => { return axios.post( method, { ...data, paramsSerializer: PARAMS_SERIALIZER }, { headers: { "X-CSRFToken": getCookie( document, { name: "csrftoken", type: STRING } ) } } ) }, e => console.warn( "API POST: ", e ) )
        return res.data
    }

    const postFormData = async( method, data = {}, options = {} ) =>
    {
        const res = await axios.post( method, data, { headers: { "X-CSRFToken": getCookie( document, { name: "csrftoken", type: STRING } ), "Content-Type": "multipart/form-data; boundary=----WebKitFormBoundary7MA4YWxkTrZu0gW" } } )
        return res.data
    }

    const put = async( method, data = {}, options = {} ) =>
    {
        const res = await tryR( () => { return axios.put( method, { ...data, paramsSerializer: PARAMS_SERIALIZER }, { headers: { "X-CSRFToken": getCookie( document, { name: "csrftoken", type: STRING } ) } } ) }, e => console.warn( "API POST: ", e ) )
        return res.data
    }

    const del = async( method, data = {}, options = {} ) =>
    {
        const res = await tryR( () => { return axios.delete( method, { ...data, paramsSerializer: PARAMS_SERIALIZER, headers: { "X-CSRFToken": getCookie( document, { name: "csrftoken", type: STRING } ) } } ) }, e => console.warn( "API POST: ", e ) )
        return res.data
    }

    return { get, post, postFormData, put, del }
}
