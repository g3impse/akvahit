import {
    ARTICLES
} from "./blog/types/const.js"

export function isArticles( path )
{
    return path.includes( ARTICLES )
}
