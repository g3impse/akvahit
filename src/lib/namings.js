/* eslint-disable import/export */

export * from "../dtaui/lib/namings.js"

export function cleanPhone( phone )
{
    return phone ? phone.replace( /[^+0-9]/g, "" ) : ""
}

export function prepositionV( title )
{
    return /^В|^в/.test( title ) && !/^Во|^во/.test( title ) ? `во ${ title }` : `в ${ title }`
}
