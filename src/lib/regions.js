import { urlToCity } from "@/lib/url"
import { PRODUCT, PRODUCT_CATEGORY, REGION } from "@/lib/url/const"

export function redirectRegion( constant, o = {} )
{
    const { isRegionInUrl, urlCitySlug, route, region, redirect } = o
    let url

    switch ( constant )
    {
    case PRODUCT:
        if ( isSeo( region ) && region.slug !== urlCitySlug )
        {
            url = `/${ region.slug }${ route.fullPath.replace( `/${ urlCitySlug }`, "" ) }`
        }
        else if ( notSeo( region ) && urlCitySlug )
        {
            url = route.fullPath.replace( `/${ urlCitySlug }`, "" )
        }

        redirect ? redirect( url ) : window.history.pushState( {}, null, url )

        break
    case PRODUCT_CATEGORY:
        if ( isSeo( region ) && !isRegionInUrl )
        {
            url = `/${ region.slug }${ route.fullPath }`
        }
        else if ( isSeo( region ) && region.slug !== urlCitySlug )
        {
            url = `/${ region.slug }${ route.fullPath.replace( `/${ urlCitySlug }`, "" ) }`
        }
        else if ( notSeo( region ) && isRegionInUrl )
        {
            url = route.fullPath.replace( `/${ urlCitySlug }`, "" )
        }

        redirect ? redirect( url ) : window.history.pushState( {}, null, url )

        break
    case REGION:
        if ( region.isSet && region.slug !== urlCitySlug )
        {
            redirect( route.fullPath.replace( urlCitySlug, region.slug ) )
        }
        break
    default:
        throw new Error( "Unknown constant." )
    }
}

export function isWrongRegion( constant, o = {} )
{
    const { isRegionInUrl, city, route, regions } = o

    switch ( constant )
    {
    case PRODUCT: {
        const urlCitySlug = urlToCity( route.path )

        if ( urlCitySlug && regions.all[ urlCitySlug ] === undefined )
        {
            return true
        }
        break
    }
    case PRODUCT_CATEGORY:
        if ( isRegionInUrl && !city.length )
        {
            return true
        }
        break
    default:
        return false
    }
}

function isSeo( region )
{
    return !region.faked && !region.isDefault
}

function notSeo( region )
{
    return region.faked || region.isDefault
}
