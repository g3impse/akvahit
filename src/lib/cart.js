import { getPrice } from "@/lib/utils"

export function getTotalBaseCost( o = {} )
{
    const { products, cart } = o
    let result = 0

    if ( products && products.length )
    {
        result = products.reduce( ( x, y ) =>
        {
            if ( y.data && cart.positions[ y.id ] )
            {
                return x + y.data.basePrice * cart.positions[ y.id ].quantity
            }
            else
            {
                return x
            }
        }, 0 )

        return getPrice( result )
    }

    return null
}

export function getTotalDiscount( o = {} )
{
    const { products, cart } = o
    let result

    if ( products && products.length )
    {
        result = products.reduce( ( x, y ) =>
        {
            if ( y.data && cart.positions[ y.id ] )
            {
                return x + ( y.data.basePrice - y.data.price ) * cart.positions[ y.id ].quantity
            }
            else
            {
                return x
            }
        }, 0 )

        return result ? `- ${ getPrice( result ) }` : "Скидок нет"
    }

    return null
}

export function getTotalCost( cart )
{
    return parseFloat( Object.values( cart.positions ).reduce( ( y, x ) => y + x.quantity * x.price, 0 ) ).toFixed( 2 )
}

export function getTotalQuantity( cart )
{
    return cart ? Object.values( cart.positions ).reduce( ( y, x ) => y + x.quantity, 0 ) : 0
}

export function getProductQuantity( o = {} )
{
    const { id, cart } = o
    return cart.positions[ id ].quantity
}

export function isCheckoutAvailable( cart )
{
    return !Object.keys( cart.positions ).length || parseFloat( cart.totalCost ) < parseFloat( cart.minCost )
}
