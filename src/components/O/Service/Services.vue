<template>
    <c>
        <div :class="$style.Services">
            <t
                type="h2"
                design="h2-secondary"
                v-html="data.title"
            />

            <div class="grid grid-cols-2 md:grid-cols-3 gap-6 xl:gap-12">
                <AButton
                    v-for="(item, i) in services.slice(0, 6)"
                    :key="`services-${i}`"
                    design="custom"
                    :class="$style.card"
                    :link="link( item.fslug )"
                >
                    <BaseImage
                        v-if="item.image && item.image.imageSrc"
                        :src="item.image.imageSrc"
                        :ext="[item.image.imageTypes, 'png']"
                        :class="$style.image"
                    />

                    <t class="block mt-8 font-condensed text-text-landing uppercase font-bold text-center md:text-16 xl:text-18">
                        {{ item.title }}
                    </t>
                </AButton>
            </div>

            <AButton
                :link="servicesLink"
                design="custom"
                class="!font-condensed uppercase font-bold text-white text-22 flex items-center justify-center bg-secondary !rounded-[6px] xl:hover:bg-blue-500 transition-colors duration-300 mt-16 !w-full md:!w-[32rem] mx-auto h-20"
            >
                смотреть все услуги
            </AButton>
        </div>
    </c>
</template>

<script>
import { SERVICE, SERVICES } from "@/lib/url/const"

export default {
    props:
    {
        data: {
            type   : Object,
            default: () => ( {} )
        },

        services: {
            type   : Array,
            default: () => []
        }
    },

    computed: {
        servicesLink()
        {
            return this.$getUrl( SERVICES )
        }
    },

    methods:
    {
        link( fslug )
        {
            return this.$getUrl( SERVICE, { fslug } )
        }
    }
}
</script>

<style lang="scss" module>
.Services {
    @apply mx-2 md:mx-4 xl:mx-0 2xl:mx-[23rem];
}

.card {
    @apply flex flex-col items-center border border-stroke p-6 xl:p-12 cursor-pointer;
    transition: box-shadow .3s ease;

    &:hover {
        @screen xl {
            box-shadow: 0 0 30px rgba(112, 112, 112, 0.2);
        }
    }
}

.image {
    @apply w-[12rem] h-[12rem] md:w-[16rem] md:h-[16rem];
    object-fit: contain;
}
</style>
