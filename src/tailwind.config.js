module.exports = {
    mode : "jit",
    purge: [
        "./components/**/*.{vue,js}",
        "./layouts/**/*.vue",
        "./pages/**/*.vue",
        "./plugins/**/*.{js,ts}",
        "./nuxt.config.{js,ts}"
    ],
    darkMode: false, // or 'media' or 'class'
    theme   : {
        screens: {
            md   : "768px",
            xl   : "1280px",
            "2xl": "1800px"
        },
        fontFamily: {
            circe    : [ "Circe", "sans-serif" ],
            gotham   : [ "Gotham", "sans-serif" ],
            roboto   : [ "Roboto", "sans-serif" ],
            condensed: [ "RobotoCondensed", "sans-serif" ]
        },
        fontSize: {
            10: "10px",
            12: "12px",
            13: "13px",
            14: "14px",
            15: "15px",
            16: "16px",
            17: "17px",
            18: "18px",
            20: "20px",
            22: "22px",
            24: "24px",
            26: "26px",
            28: "28px",
            30: "30px",
            32: "32px",
            34: "34px",
            36: "36px"
        },
        colors: {
            primary  : "#FF3E3D",
            secondary: "#025273",
            brown    : "#3E3E40",

            text: {
                main   : "#0E1F3D",
                low    : "#404356",
                landing: "#464646"
            },

            gray: {
                50 : "#F8F8F8",
                100: "#FBFBFB",
                150: "#E6E9F1",
                200: "#ABABA8",
                250: "#E4E4E4",
                300: "#C4C4C4",
                350: "#DCD8D8",
                400: "#9C9C9C",
                450: "#909090",
                500: "#696969",
                550: "#8C8C8C",
                600: "#919CB5",
                700: "#909090",
                750: "#6B6B6B",
                800: "#AEAEAE",
                850: "#868686",
                900: "#3D4258",
                950: "#7C7C7C"
            },

            red: {
                100: "#FFC8C8",
                500: "#F23130"
            },

            orange: "#F2994A",

            blue: {
                100: "#F7F5F7",
                500: "#003E58",
                800: "#262A3C",
                900: "#071F35"
            },

            green: "#219653",

            border     : "#E9E9E9",
            stroke     : "#DFDFDF",
            background : "#F4F6FB",
            black      : "#000000",
            white      : "#FFFFFF",
            transparent: "transparent",

            landing: {
                gray: {
                    100: "#F0F0F0",
                    150: "#E5E5E5",
                    200: "#BDBDBD",
                    300: "#EEEEEE",
                    400: "#DCDCDC",
                    500: "#959595",
                    600: "#ECECEC",
                    700: "#777777",
                    900: "#999999"
                }
            },

            admin: {
                green: {
                    200: "#338A34",
                    500: "#14700F"
                }
            }
        },
        letterSpacing: {
            wide  : ".01em",
            widest: ".1em"
        },
        container: {
            center : true,
            padding: {
                DEFAULT: "1rem",
                md     : "2rem",
                xl     : "6rem",
                "2xl"  : "7rem"
            }
        }
    },
    variants: {
        extend: {}
    },
    plugins: []
}
