/* eslint-disable import/namespace */
import * as allConsts from "../lib/url/const.js"
import allRoutes from "../.nuxt/routes.json"
import { getConst, getUrlMask } from "../lib/url.js"

const consts = {}
const constsNames = []
for ( const key of Object.keys( allConsts ) )
{
    const res = getUrlMask( allConsts[ key ] )
    if ( res )
    {
        consts[ key ] = res
    }
    constsNames.push( key )
}

const routes = [ ...new Set( allRoutes.map( ( e ) =>
{
    let res = e.path.replace( /:region|:id|:guid|:slug|:fslug|\?|\*/g, "" )
    if ( res.slice( -2 ) === "//" )
    {
        res = res.slice( 0, -1 )
    }
    if ( res.slice( -1 ) !== "/" )
    {
        res = `${res}/`
    }
    return res
} ).filter( e => !e.startsWith( "/:city" ) ) ) ]

const bodyParser = require( "body-parser" )
const app = require( "express" )()

app.use( bodyParser.json() )
app.get( "/sitemap/", ( req, res ) =>
{
    res.json( {
        data: {
            ...consts,
            pages: routes
        }
    } )
} )

app.get( "/consts/", ( req, res ) =>
{
    res.json( {
        data: constsNames
    } )
} )

app.post( "/parse/", ( req, res ) =>
{
    const code = getConst( req.body.url )

    if ( code )
    {
        res.json( {
            code
        } )
    }
    else
    {
        res.json( {
            error: "Page not found."
        } )
    }
} )

module.exports = app
