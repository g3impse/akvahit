export default {
    head:
    {
        title    : "АкваХит",
        htmlAttrs:
        {
            lang: "ru"
        },
        meta:
        [
            { charset: "utf-8" },
            { name: "viewport", content: "width=device-width, initial-scale=1, maximum-scale=1" },
            { hid: "description", name: "description", content: "" },
            { name: "format-detection", content: "telephone=no" }
        ],
        link:
        [
            { rel: "icon", type: "image/png", href: "/favicon-0.png", sizes: "16x16" },
            { rel: "icon", type: "image/png", href: "/favicon-1.png", sizes: "24x24" },
            { rel: "icon", type: "image/png", href: "/favicon-2.png", sizes: "32x32" },
            { rel: "icon", type: "image/png", href: "/favicon-3.png", sizes: "48x48" }
        ]
    },

    serverMiddleware: [
        { path: "/url", handler: "~/serverMiddleware/url.js" }
    ],

    plugins:
    [
        { src: "@/plugins/vue-slider.js", mode: "client" },
        { src: "@/plugins/ymaps.js", mode: "client" },
        "@/plugins/loading-brain",
        "@/plugins/api.js",
        "@/plugins/globalFunctions.js",
        "@/plugins/index.client.js",
        "@/plugins/jsonld.js"
    ],

    css:
    [
        "@/assets/css/tailwind.css",
        "@/assets/css/swiper.scss",
        "@/assets/css/transitions.css"
    ],

    loading: "@/components/O/Loader.vue"
}
